module.exports = {
  pattern: '^(master|main|staging|production)$|^(bump|feat|fix|rel(?:ease)?)/.+$',
  errorMsg:
    '🤨 Your branch does not respect standards. Please rename it with: `git branch -m <current> <new>`',
};
