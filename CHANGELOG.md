## [0.90.5](https://gitlab.com/getanthill/datastore/compare/v0.90.4...v0.90.5) (2025-01-31)


### Bug Fixes

* **walk:** remove useless calls to min and max version methods ([e12d072](https://gitlab.com/getanthill/datastore/commit/e12d0723904e4f5381c329f468f3f2cb0ca6a666))

## [0.90.4](https://gitlab.com/getanthill/datastore/compare/v0.90.3...v0.90.4) (2024-12-04)


### Bug Fixes

* **walk:** ensure to retrieve find pagination fields for logic ([4cea5a0](https://gitlab.com/getanthill/datastore/commit/4cea5a0033373021bc59dfbec3de76e78e070852))

## [0.90.3](https://gitlab.com/getanthill/datastore/compare/v0.90.2...v0.90.3) (2024-12-02)


### Bug Fixes

* **stream:** adding metrics to monitor the automatic change logs creation flow ([d07e480](https://gitlab.com/getanthill/datastore/commit/d07e4802f595c169d0de62e02e0c72b9b67e4343))

## [0.90.2](https://gitlab.com/getanthill/datastore/compare/v0.90.1...v0.90.2) (2024-11-21)


### Bug Fixes

* **sdk:** adding test on walkMulti chunkSize ([89c778b](https://gitlab.com/getanthill/datastore/commit/89c778b9b5fd4782e554108c5bdc5601ec420ecb))
* **sdk:** fix walkMulti is_exhaustive termination error ([6e72b3f](https://gitlab.com/getanthill/datastore/commit/6e72b3ff09e65bdb83f6fd6dd65a65c219f7ab52))

## [0.90.1](https://gitlab.com/getanthill/datastore/compare/v0.90.0...v0.90.1) (2024-11-20)


### Bug Fixes

* **stream:** supports multiple connection loss on streams ([5b1e180](https://gitlab.com/getanthill/datastore/commit/5b1e180d8c4152f6b84b630d3ef507f4e731a551))

# [0.90.0](https://gitlab.com/getanthill/datastore/compare/v0.89.6...v0.90.0) (2024-11-05)


### Features

* **sdk:** add chunk size opts and reduce fallback value to 5 ([f9e4cd5](https://gitlab.com/getanthill/datastore/commit/f9e4cd5b16b718db8b27ab8209576c7bacad0427))

## [0.89.6](https://gitlab.com/getanthill/datastore/compare/v0.89.5...v0.89.6) (2024-10-31)


### Bug Fixes

* **stream:** supports reconnect on changeStream after connection heartbeat is exhausted ([797e3e3](https://gitlab.com/getanthill/datastore/commit/797e3e392fa07931f999e30fe4795ed8720bc2e6))

## [0.89.5](https://gitlab.com/getanthill/datastore/compare/v0.89.4...v0.89.5) (2024-10-17)


### Bug Fixes

* **indexes:** ensures `_id` field indexing is added during the create index ([ad7914c](https://gitlab.com/getanthill/datastore/commit/ad7914ce2c82d57c126da64a32d1127db11e4e22))
* **sdk:** fix the max page size value in export data logic ([6786f68](https://gitlab.com/getanthill/datastore/commit/6786f687d7f1b7a2d787d81469dff6a132a19dcf))
* **walk:** fix order of events within the same milliseconds based on version ([b7c2e21](https://gitlab.com/getanthill/datastore/commit/b7c2e2125919f730686325f82379a76131a09fdc))

## [0.89.4](https://gitlab.com/getanthill/datastore/compare/v0.89.3...v0.89.4) (2024-10-16)


### Bug Fixes

* **sdk:** retries requests with `_q` param ([3244f58](https://gitlab.com/getanthill/datastore/commit/3244f58adc6a7a78ff848f6040f52e0d4f02e5a8))

## [0.89.3](https://gitlab.com/getanthill/datastore/compare/v0.89.2...v0.89.3) (2024-10-14)


### Bug Fixes

* remove postinstall step ([74ad034](https://gitlab.com/getanthill/datastore/commit/74ad0340ca7db581ae83be78131c1d8bc69ce5e6))

## [0.89.2](https://gitlab.com/getanthill/datastore/compare/v0.89.1...v0.89.2) (2024-10-14)


### Bug Fixes

* **walk:** limits the number of parallel batches to 10 ([aba86ec](https://gitlab.com/getanthill/datastore/commit/aba86ece9779df40b303a5d5188664f0295a69ed))

## [0.89.1](https://gitlab.com/getanthill/datastore/compare/v0.89.0...v0.89.1) (2024-10-13)


### Bug Fixes

* **fhe:** supports multiple handlers with and without FHE enabled ([9bc1b85](https://gitlab.com/getanthill/datastore/commit/9bc1b85d578a21cf8a6e9f4f134f9f6e61faeaf1))

# [0.89.0](https://gitlab.com/getanthill/datastore/compare/v0.88.2...v0.89.0) (2024-10-12)


### Features

* supports of fully homomorphic encryption computation (alpha) ([f8c46a3](https://gitlab.com/getanthill/datastore/commit/f8c46a360acf2f610a8c9fa403767cdc33cd40ac))

## [0.88.2](https://gitlab.com/getanthill/datastore/compare/v0.88.1...v0.88.2) (2024-10-09)


### Bug Fixes

* **telemetry:** bump telemetry to get otel instrumentation metrics ([3b8b7a1](https://gitlab.com/getanthill/datastore/commit/3b8b7a1bbb991b37d88752e58c5c20b73853ad39))

## [0.88.1](https://gitlab.com/getanthill/datastore/compare/v0.88.0...v0.88.1) (2024-09-27)


### Bug Fixes

* **api:** fixes ISO date timetravel ([a2bd94b](https://gitlab.com/getanthill/datastore/commit/a2bd94bcd3d238167d7686f302a4683f8e903a4d))

# [0.88.0](https://gitlab.com/getanthill/datastore/compare/v0.87.1...v0.88.0) (2024-09-26)


### Features

* **sdk:** allows to define a max pageSize in walk ([05e9527](https://gitlab.com/getanthill/datastore/commit/05e95275c3449ce76d13f72e2c4983df6349ed01))

## [0.87.1](https://gitlab.com/getanthill/datastore/compare/v0.87.0...v0.87.1) (2024-09-16)


### Bug Fixes

* **api:** remove `upsert` property from OpenAPI specification ([586f1ef](https://gitlab.com/getanthill/datastore/commit/586f1efc2b8aa3f966a60d8e193536898efbf884))

# [0.87.0](https://gitlab.com/getanthill/datastore/compare/v0.86.3...v0.87.0) (2024-08-28)


### Features

* **runner:** allows to release scalable concurrent runners ([2031aba](https://gitlab.com/getanthill/datastore/commit/2031aba3ed3d8dcfc473329169572e9bddd65f71))

## [0.86.3](https://gitlab.com/getanthill/datastore/compare/v0.86.2...v0.86.3) (2024-08-27)


### Bug Fixes

* **indexes:** fix location of `correlation_id_unicity` index in indexes initialization ([a6f75f3](https://gitlab.com/getanthill/datastore/commit/a6f75f3a9629f72a19c6702aaa4719dd29127e38))

## [0.86.2](https://gitlab.com/getanthill/datastore/compare/v0.86.1...v0.86.2) (2024-07-26)


### Bug Fixes

* **setup:** change temporary database naming ([87f66d5](https://gitlab.com/getanthill/datastore/commit/87f66d5d422ac4e7e10469bb18fdc686d189ca84))

## [0.86.1](https://gitlab.com/getanthill/datastore/compare/v0.86.0...v0.86.1) (2024-07-23)


### Bug Fixes

* enforce the order of documents returned in find requests ([b81400e](https://gitlab.com/getanthill/datastore/commit/b81400e27616525647b0a8cfb1b20262caa5de00))

# [0.86.0](https://gitlab.com/getanthill/datastore/compare/v0.85.5...v0.86.0) (2024-07-16)


### Features

* **indexes:** improves the generation of models indexes ([864b8c5](https://gitlab.com/getanthill/datastore/commit/864b8c53ed565a838222c9c5c5a5a0d18ed053a6))

## [0.85.5](https://gitlab.com/getanthill/datastore/compare/v0.85.4...v0.85.5) (2024-06-26)


### Bug Fixes

* **walk:** fix behavior om walk multi for events with the exact same created timestamp ([1f8ceb1](https://gitlab.com/getanthill/datastore/commit/1f8ceb11bceb9141255ab5d14c4212810729147e))

## [0.85.4](https://gitlab.com/getanthill/datastore/compare/v0.85.3...v0.85.4) (2024-06-20)


### Bug Fixes

* **sdk:** avoid querying events with entities `updated_at` field ([fcaed46](https://gitlab.com/getanthill/datastore/commit/fcaed4650a31d3d83c37a6e147bfa6985b9d639b))

## [0.85.3](https://gitlab.com/getanthill/datastore/compare/v0.85.2...v0.85.3) (2024-06-12)


### Bug Fixes

* **runner:** fix runner graceful timeout on successful replay ([985721f](https://gitlab.com/getanthill/datastore/commit/985721fef7952ad2cdf87cc75ac88be807d82c5e))

## [0.85.2](https://gitlab.com/getanthill/datastore/compare/v0.85.1...v0.85.2) (2024-06-12)


### Bug Fixes

* apply exitTimeout delay when exiting replay ([a60ff9a](https://gitlab.com/getanthill/datastore/commit/a60ff9a9970a34e61762493298435b02f007ebf4))

## [0.85.1](https://gitlab.com/getanthill/datastore/compare/v0.85.0...v0.85.1) (2024-05-30)


### Bug Fixes

* **walk:** order equals in time events ad entities by version ([cce0718](https://gitlab.com/getanthill/datastore/commit/cce0718a08f2b9b84d8fea1841c8360457eb7175))

# [0.85.0](https://gitlab.com/getanthill/datastore/compare/v0.84.0...v0.85.0) (2024-05-29)


### Features

* **observability:** adding metrics on cli and sdk ([db4b579](https://gitlab.com/getanthill/datastore/commit/db4b579cced7d74e66c1b03602136e911fe9865a))

# [0.84.0](https://gitlab.com/getanthill/datastore/compare/v0.83.0...v0.84.0) (2024-05-27)


### Features

* **metrics:** align metrics accross all the application ([9b0c224](https://gitlab.com/getanthill/datastore/commit/9b0c22419f78cdd80f3960c4ce7002136ed4c25e))

# [0.83.0](https://gitlab.com/getanthill/datastore/compare/v0.82.6...v0.83.0) (2024-05-22)


### Features

* implements application level processing metrics ([9e4c547](https://gitlab.com/getanthill/datastore/commit/9e4c5470f6dbbe5e81d3c9afa4526a6e62f3ac02))

## [0.82.6](https://gitlab.com/getanthill/datastore/compare/v0.82.5...v0.82.6) (2024-05-22)


### Bug Fixes

* bump dependencies ([aa72a8a](https://gitlab.com/getanthill/datastore/commit/aa72a8ae384ca78970a952aec23aac4eae1486e3))

## [0.82.5](https://gitlab.com/getanthill/datastore/compare/v0.82.4...v0.82.5) (2024-05-22)


### Bug Fixes

* **walk:** fix wrong behavor on batch generation on last iteration ([3a2a593](https://gitlab.com/getanthill/datastore/commit/3a2a593155f640da14e316d1f081b13a55745064))

## [0.82.4](https://gitlab.com/getanthill/datastore/compare/v0.82.3...v0.82.4) (2024-05-16)


### Bug Fixes

* **amqp:** supports multiple AMQP connection URLs ([44d4f46](https://gitlab.com/getanthill/datastore/commit/44d4f463c3f639dd327feabaae89071b6cd1a879))

## [0.82.3](https://gitlab.com/getanthill/datastore/compare/v0.82.2...v0.82.3) (2024-05-10)


### Bug Fixes

* **walk:** fix bah behavior on `handle_in_order` walk ([8e9e9bf](https://gitlab.com/getanthill/datastore/commit/8e9e9bf33d4669fbbd4b55756bbc63dfbf7cffd3))

## [0.82.2](https://gitlab.com/getanthill/datastore/compare/v0.82.1...v0.82.2) (2024-05-08)


### Bug Fixes

* **walk:** allows to handle results in parallel or in order ([ce37ebd](https://gitlab.com/getanthill/datastore/commit/ce37ebd1c9f19701b3dcb51d76e3aba366d9fdca))

## [0.82.1](https://gitlab.com/getanthill/datastore/compare/v0.82.0...v0.82.1) (2024-05-08)


### Bug Fixes

* **walk:** apply the parallelism concurrency on results from the same model and datastore ([3daae90](https://gitlab.com/getanthill/datastore/commit/3daae909f4682bb1586b116b5c433bf048a1a50a))

# [0.82.0](https://gitlab.com/getanthill/datastore/compare/v0.81.0...v0.82.0) (2024-05-07)


### Features

* **walk:** allows to replay events in temporal order with the parallelism constraint ([440f3b2](https://gitlab.com/getanthill/datastore/commit/440f3b2d93ca7e143e4020dfb6c45d7cd4c6b8ee))

# [0.81.0](https://gitlab.com/getanthill/datastore/compare/v0.80.3...v0.81.0) (2024-04-19)


### Bug Fixes

* adds missing headers in CORS for replay ([29f21e6](https://gitlab.com/getanthill/datastore/commit/29f21e6912e36c25b43fed499a50d1d34df0369b))
* update logic for can and can not create entities ([a1105ff](https://gitlab.com/getanthill/datastore/commit/a1105ff9f9ef51b1b6b432bd831d3ccb8fa740ee))


### Features

* allows to support upsert on apply events ([cbb8231](https://gitlab.com/getanthill/datastore/commit/cbb823154bf7f6de484a4076c168feb1b3b7c860))

## [0.80.3](https://gitlab.com/getanthill/datastore/compare/v0.80.2...v0.80.3) (2024-02-23)


### Bug Fixes

* **sdk:** retries some requests on SDK ([d077313](https://gitlab.com/getanthill/datastore/commit/d077313ba53544b3db196298e5e7126485d7152f))

## [0.80.2](https://gitlab.com/getanthill/datastore/compare/v0.80.1...v0.80.2) (2024-02-21)


### Bug Fixes

* set the deleted flag to false during archive process ([7d2b8a0](https://gitlab.com/getanthill/datastore/commit/7d2b8a0ccd2a8aff972f31b87367e14a5e92eb72))

## [0.80.1](https://gitlab.com/getanthill/datastore/compare/v0.80.0...v0.80.1) (2024-02-19)


### Bug Fixes

* **walk:** use walkMulti logic in the datastore client ([8f5f080](https://gitlab.com/getanthill/datastore/commit/8f5f080af2eec4e08fb6751f88c7704f971af1b4))

# [0.80.0](https://gitlab.com/getanthill/datastore/compare/v0.79.8...v0.80.0) (2024-02-19)


### Features

* **pg:** allows to exclude encrypted data in pg sync ([612ef6e](https://gitlab.com/getanthill/datastore/commit/612ef6e57e703be53a2c010120d96b2400174f05))
* **pg:** removes encrypted data from pg sync ([ff3be6f](https://gitlab.com/getanthill/datastore/commit/ff3be6fe4f44f75a1fc14cc955ceaf8babf96050))

## [0.79.8](https://gitlab.com/getanthill/datastore/compare/v0.79.7...v0.79.8) (2024-02-19)


### Bug Fixes

* **walk:** remove cursor pagination in sdk walk due to pagination error ([eea7052](https://gitlab.com/getanthill/datastore/commit/eea705264fb82563a1ab0e18d56d0743c5ae4b7f))

## [0.79.7](https://gitlab.com/getanthill/datastore/compare/v0.79.6...v0.79.7) (2024-02-19)


### Bug Fixes

* keep the value of the `is_readonly` flag during unarchive process ([08539f9](https://gitlab.com/getanthill/datastore/commit/08539f94a8125c3f39b4ab5e3f7251d566bc0cae))

## [0.79.6](https://gitlab.com/getanthill/datastore/compare/v0.79.5...v0.79.6) (2024-02-16)


### Bug Fixes

* **walk:** fix walkMulti with cursor pagination ([9da6f0f](https://gitlab.com/getanthill/datastore/commit/9da6f0fe426c42499ace38962d016f204d589f9a))

## [0.79.5](https://gitlab.com/getanthill/datastore/compare/v0.79.4...v0.79.5) (2024-02-08)


### Bug Fixes

* **runner:** improve runner processing feedback ([6d3e161](https://gitlab.com/getanthill/datastore/commit/6d3e161f8bdf8599a498ef3be4bd948f68198e1f))

## [0.79.4](https://gitlab.com/getanthill/datastore/compare/v0.79.3...v0.79.4) (2024-02-08)


### Bug Fixes

* improve overall find performances on high data volumes ([88388c0](https://gitlab.com/getanthill/datastore/commit/88388c03b4edfbb614a1acc65af6108f24593194))

## [0.79.3](https://gitlab.com/getanthill/datastore/compare/v0.79.2...v0.79.3) (2024-02-02)


### Bug Fixes

* extend data for obligations ([67375df](https://gitlab.com/getanthill/datastore/commit/67375df564c027dc25dae08e95a9673e1d9cb632))

## [0.79.2](https://gitlab.com/getanthill/datastore/compare/v0.79.1...v0.79.2) (2024-02-01)


### Bug Fixes

* allows to skip and restrict authorization to some models only ([9b331d5](https://gitlab.com/getanthill/datastore/commit/9b331d558c17cf57ae3b96ac038d74b153ad5365))

## [0.79.1](https://gitlab.com/getanthill/datastore/compare/v0.79.0...v0.79.1) (2024-02-01)


### Bug Fixes

* allows to configre maxAge on specific cookies individually ([ad95cd3](https://gitlab.com/getanthill/datastore/commit/ad95cd3273b65f05b57ca1a3c0aa6bd98fcadf5d))

# [0.79.0](https://gitlab.com/getanthill/datastore/compare/v0.78.3...v0.79.0) (2024-01-31)


### Features

* authorization layer ([f5323e2](https://gitlab.com/getanthill/datastore/commit/f5323e26927e3764248d32099990e2fa0c48ef6c))

## [0.78.3](https://gitlab.com/getanthill/datastore/compare/v0.78.2...v0.78.3) (2024-01-20)


### Bug Fixes

* adjust units for cookie max age ([908f82b](https://gitlab.com/getanthill/datastore/commit/908f82bf0a898e1d013774b0d87ea722855c551f))

## [0.78.2](https://gitlab.com/getanthill/datastore/compare/v0.78.1...v0.78.2) (2024-01-20)


### Bug Fixes

* allows to configure precisely auth cookies ([ed2b41a](https://gitlab.com/getanthill/datastore/commit/ed2b41ac64fcfb4610c27be9811bede81dd7ceb2))

## [0.78.1](https://gitlab.com/getanthill/datastore/compare/v0.78.0...v0.78.1) (2024-01-18)


### Bug Fixes

* alllows cross site cookie setting ([ce77025](https://gitlab.com/getanthill/datastore/commit/ce7702581047b595094c4a79255d8df42a11202c))

# [0.78.0](https://gitlab.com/getanthill/datastore/compare/v0.77.1...v0.78.0) (2024-01-18)


### Features

* **auth:** allows to authenticate with secured cookies ([82aac41](https://gitlab.com/getanthill/datastore/commit/82aac419e361fff6931ea7cffb1ab84f7738bc60))

## [0.77.1](https://gitlab.com/getanthill/datastore/compare/v0.77.0...v0.77.1) (2024-01-16)


### Bug Fixes

* **build:** bump node version to 20 in CI ([b2ed36a](https://gitlab.com/getanthill/datastore/commit/b2ed36a566403fd2101f57230e0cc412b107b573))
* **sse:** fix headers to disable stream compression ([5f1b55f](https://gitlab.com/getanthill/datastore/commit/5f1b55f1390176502389743e95c38c88ddb9af42))

# [0.77.0](https://gitlab.com/getanthill/datastore/compare/v0.76.4...v0.77.0) (2024-01-10)


### Features

* **schema:** schemas generation simplified with custom events encrypted fields supported ([e14d9f0](https://gitlab.com/getanthill/datastore/commit/e14d9f09c9248a99d5fa4314a2d32411af25e656))

## [0.76.4](https://gitlab.com/getanthill/datastore/compare/v0.76.3...v0.76.4) (2024-01-10)


### Bug Fixes

* **query:** supports number values date mapping in deepCoerce ([1dfabf4](https://gitlab.com/getanthill/datastore/commit/1dfabf46f03bd3fed66154957f131415bb1ab487))

## [0.76.3](https://gitlab.com/getanthill/datastore/compare/v0.76.2...v0.76.3) (2024-01-06)


### Bug Fixes

* **release:** release ([dacf599](https://gitlab.com/getanthill/datastore/commit/dacf59984655c51d836514719a37ecdcff185a84))

## [0.76.2](https://gitlab.com/getanthill/datastore/compare/v0.76.1...v0.76.2) (2024-01-06)


### Bug Fixes

* allows to define retry duration per applied event ([8afc6cd](https://gitlab.com/getanthill/datastore/commit/8afc6cdd316b71b3c795a387e962a64bf9dcaa3d))

## [0.76.1](https://gitlab.com/getanthill/datastore/compare/v0.76.0...v0.76.1) (2024-01-06)


### Bug Fixes

* allows to define options on every event handler ([bd5a41e](https://gitlab.com/getanthill/datastore/commit/bd5a41e2ced8955ef007ab1c975bed9ee2a60394))

# [0.76.0](https://gitlab.com/getanthill/datastore/compare/v0.75.3...v0.76.0) (2024-01-05)


### Features

* exposes a utility function to serialize handlers in models ([d1ccb95](https://gitlab.com/getanthill/datastore/commit/d1ccb95a27722831b006002424c47207adfefd2e))

## [0.75.3](https://gitlab.com/getanthill/datastore/compare/v0.75.2...v0.75.3) (2024-01-05)


### Bug Fixes

* **runner:** allows to define a specific query on stream ([e131a11](https://gitlab.com/getanthill/datastore/commit/e131a11c230bd900e1162d41e1eaf5198b28ee3c))
* **sdk:** remove cyclic dependency on SDK Core request error throwing ([f79cc65](https://gitlab.com/getanthill/datastore/commit/f79cc65eb5f44087151a302ff315c9e6928bd23e))

## [0.75.2](https://gitlab.com/getanthill/datastore/compare/v0.75.1...v0.75.2) (2024-01-04)


### Bug Fixes

* **model:** allows to disable default events from API ([3bcf507](https://gitlab.com/getanthill/datastore/commit/3bcf50739c23bbc92b47bdfe0e8475d09245438c))
* **query:** allows to perform raw $exists on dates ([dc5058d](https://gitlab.com/getanthill/datastore/commit/dc5058d0ea4e618db36aee8939320ed61372ef32))

## [0.75.1](https://gitlab.com/getanthill/datastore/compare/v0.75.0...v0.75.1) (2024-01-03)


### Bug Fixes

* supports is_readonly, is_archived and is_deleted fields in events validation ([ad21c34](https://gitlab.com/getanthill/datastore/commit/ad21c343f1ac5248e7c85f4d9c4f3a469af7d60f))

# [0.75.0](https://gitlab.com/getanthill/datastore/compare/v0.74.4...v0.75.0) (2024-01-03)


### Features

* **perf:** dates validation performance improved ([419955d](https://gitlab.com/getanthill/datastore/commit/419955da5c302ef7a301981072b1e9eaa7341cb3))

## [0.74.4](https://gitlab.com/getanthill/datastore/compare/v0.74.3...v0.74.4) (2024-01-03)


### Bug Fixes

* supports events validations with custom Date ([ed5dec4](https://gitlab.com/getanthill/datastore/commit/ed5dec4e9a1f17daa414eaa1c2d6087d60c1ab13))

## [0.74.3](https://gitlab.com/getanthill/datastore/compare/v0.74.2...v0.74.3) (2024-01-03)


### Bug Fixes

* **reducers:** fix state arrays behaviors with replacement ([3e4ee80](https://gitlab.com/getanthill/datastore/commit/3e4ee803111a62d6fef4549faa1fbb7fb03e8c1d))

## [0.74.2](https://gitlab.com/getanthill/datastore/compare/v0.74.1...v0.74.2) (2024-01-03)


### Bug Fixes

* **indexes:** improve the logic to update indexes on a model ([43d8907](https://gitlab.com/getanthill/datastore/commit/43d8907fd2d024dfa8d56d86bf1cb7a82d1e9b61))

## [0.74.1](https://gitlab.com/getanthill/datastore/compare/v0.74.0...v0.74.1) (2024-01-03)


### Bug Fixes

* **ajv:** uses cached schema validation for events and entities ([fe47aa7](https://gitlab.com/getanthill/datastore/commit/fe47aa7e8178da8494ebd82883e218339d7cd093))

# [0.74.0](https://gitlab.com/getanthill/datastore/compare/v0.73.5...v0.74.0) (2024-01-02)


### Features

* required fields better support ([fcdd721](https://gitlab.com/getanthill/datastore/commit/fcdd721b1a9882f301500309531a484f706513c1))

## [0.73.5](https://gitlab.com/getanthill/datastore/compare/v0.73.4...v0.73.5) (2023-12-27)


### Bug Fixes

* bump dependencies ([e197e5e](https://gitlab.com/getanthill/datastore/commit/e197e5e672ec2f708b8e6597baaab3c808e2ea52))

## [0.73.4](https://gitlab.com/getanthill/datastore/compare/v0.73.3...v0.73.4) (2023-12-15)


### Bug Fixes

* improve performance on entities decrypt ([351b463](https://gitlab.com/getanthill/datastore/commit/351b4637aa78133baca24c66df8d83498d8f5628))

## [0.73.3](https://gitlab.com/getanthill/datastore/compare/v0.73.2...v0.73.3) (2023-12-07)


### Bug Fixes

* **pg:** fix namespace support during tables initialization ([777453c](https://gitlab.com/getanthill/datastore/commit/777453c3c26e90b1f062cc70a4f05d961029d78c))

## [0.73.2](https://gitlab.com/getanthill/datastore/compare/v0.73.1...v0.73.2) (2023-10-13)


### Bug Fixes

* **amqp:** exclude nullable field from required properties ([381bcf6](https://gitlab.com/getanthill/datastore/commit/381bcf6dda2b4661d897164a9473f42a4d4adf65))
* **runner:** allows to specify if the trigger query is a JSON Schema already ([49f8839](https://gitlab.com/getanthill/datastore/commit/49f8839c67a31cc0b2e4856061c6d043daebf909))
* **sdk:** specify if the SDK connector is HTTP or AMQP ([4b60bae](https://gitlab.com/getanthill/datastore/commit/4b60baed45481826292494f39447c195050f1866))

## [0.73.1](https://gitlab.com/getanthill/datastore/compare/v0.73.0...v0.73.1) (2023-10-13)


### Bug Fixes

* **runner:** fix order of events processing under high volume ([2b7857b](https://gitlab.com/getanthill/datastore/commit/2b7857b58ebbe31acb39226361ca92b4fbe872c2))

# [0.73.0](https://gitlab.com/getanthill/datastore/compare/v0.72.3...v0.73.0) (2023-10-12)


### Features

* **runner:** supports initial sampling parallel requests limit ([15feb16](https://gitlab.com/getanthill/datastore/commit/15feb16fcec1e07b47a5c982787b781f0be89d44))

## [0.72.3](https://gitlab.com/getanthill/datastore/compare/v0.72.2...v0.72.3) (2023-10-12)


### Bug Fixes

* **runner:** fix min event processing ([e4fc9e7](https://gitlab.com/getanthill/datastore/commit/e4fc9e74a091621561b44d7ff3454cb0450bfd2a))

## [0.72.2](https://gitlab.com/getanthill/datastore/compare/v0.72.1...v0.72.2) (2023-10-12)


### Bug Fixes

* **runner:** ensure to log processing stats only every 100 events ([60d2afa](https://gitlab.com/getanthill/datastore/commit/60d2afa044251a580ee078bbf9a99ac661931ab1))
* **runner:** ensure to process at least one event ([3010b8e](https://gitlab.com/getanthill/datastore/commit/3010b8ebd05b849da2f1e09ec6fea01b128ba401))
* **runner:** ensure to process at least one event ([42b03db](https://gitlab.com/getanthill/datastore/commit/42b03dbf4c01f1d22ef33cd6faaaf3f12c9f6b72))

## [0.72.1](https://gitlab.com/getanthill/datastore/compare/v0.72.0...v0.72.1) (2023-10-12)


### Bug Fixes

* **cli:** fix runner arguments format ([e5221c1](https://gitlab.com/getanthill/datastore/commit/e5221c1f1220609e6427e45abde08ee89e6ef9dd))

# [0.72.0](https://gitlab.com/getanthill/datastore/compare/v0.71.1...v0.72.0) (2023-10-12)


### Features

* **runner:** supports a dynamic back pressure on events processing ([998cf21](https://gitlab.com/getanthill/datastore/commit/998cf21d6d2aff560a51da2bda5479aafeae9158))

## [0.71.1](https://gitlab.com/getanthill/datastore/compare/v0.71.0...v0.71.1) (2023-10-11)


### Bug Fixes

* **dep:** bump telemetry ([6b5d0c5](https://gitlab.com/getanthill/datastore/commit/6b5d0c5f094d2e2525047ed7d51cda5eb3e715f9))

# [0.71.0](https://gitlab.com/getanthill/datastore/compare/v0.70.12...v0.71.0) (2023-10-09)


### Features

* **proj:** start the projection state with the entity metadata ([21e777e](https://gitlab.com/getanthill/datastore/commit/21e777e73e822e4a0f8ff596f057a390799f75e3))

## [0.70.12](https://gitlab.com/getanthill/datastore/compare/v0.70.11...v0.70.12) (2023-10-09)


### Bug Fixes

* **dep:** bump telemetry ([1bb3db4](https://gitlab.com/getanthill/datastore/commit/1bb3db407fe56288c84b5c0f12562f573c91af90))

## [0.70.11](https://gitlab.com/getanthill/datastore/compare/v0.70.10...v0.70.11) (2023-10-05)


### Bug Fixes

* **node:** bump to node 20 ([1462d70](https://gitlab.com/getanthill/datastore/commit/1462d70eda073311eb5f4d7a9805a5af3cd9c4e7))

## [0.70.10](https://gitlab.com/getanthill/datastore/compare/v0.70.9...v0.70.10) (2023-10-05)


### Bug Fixes

* **dep:** bump packages ([0fb1046](https://gitlab.com/getanthill/datastore/commit/0fb104651563cf30ef7e7114279e7af9219b211f))

## [0.70.9](https://gitlab.com/getanthill/datastore/compare/v0.70.8...v0.70.9) (2023-10-05)


### Bug Fixes

* **scripts:** remove deprecation warning ([3b77cfa](https://gitlab.com/getanthill/datastore/commit/3b77cfa6032b68d10a999e0805c1b8b6b06dbbf7))

## [0.70.8](https://gitlab.com/getanthill/datastore/compare/v0.70.7...v0.70.8) (2023-10-05)


### Bug Fixes

* **dep:** remove telemetry from devDependencies ([e49755f](https://gitlab.com/getanthill/datastore/commit/e49755fe0fb74dfb88484accaf0324da2f03f4d6))

## [0.70.7](https://gitlab.com/getanthill/datastore/compare/v0.70.6...v0.70.7) (2023-10-05)


### Bug Fixes

* **dep:** bump telemetry ([ae69d3c](https://gitlab.com/getanthill/datastore/commit/ae69d3c942b4afd792eacdebf247f7927a3c86d2))

## [0.70.6](https://gitlab.com/getanthill/datastore/compare/v0.70.5...v0.70.6) (2023-10-05)


### Bug Fixes

* runner exit 1 on initialization error ([a9ec5d5](https://gitlab.com/getanthill/datastore/commit/a9ec5d5ac736fb11f7751fa6f517aa2c937eaf07))

## [0.70.5](https://gitlab.com/getanthill/datastore/compare/v0.70.4...v0.70.5) (2023-09-07)


### Bug Fixes

* **log:** lower the create indexes failure log to warn ([5fb3659](https://gitlab.com/getanthill/datastore/commit/5fb3659d14db027b09270ab93593ddbb67577c41))

## [0.70.4](https://gitlab.com/getanthill/datastore/compare/v0.70.3...v0.70.4) (2023-09-04)


### Bug Fixes

* **dep:** move telemetry package in peer dependencies ([b5f1011](https://gitlab.com/getanthill/datastore/commit/b5f1011944a10d1b5c8d05e5f907140292cd26b2))

## [0.70.3](https://gitlab.com/getanthill/datastore/compare/v0.70.2...v0.70.3) (2023-09-01)


### Bug Fixes

* **dep:** bump telemetry package ([0f50315](https://gitlab.com/getanthill/datastore/commit/0f503150ba059d960a1aae75be702981f62b8171))
* **sec:** bump dependencies to fix security warnings ([72eb6e8](https://gitlab.com/getanthill/datastore/commit/72eb6e802beaddb9214d66e9f468102cb7baff78))

## [0.70.2](https://gitlab.com/getanthill/datastore/compare/v0.70.1...v0.70.2) (2023-08-31)


### Bug Fixes

* **dep:** bump dependencies ([2475462](https://gitlab.com/getanthill/datastore/commit/2475462c39d956acc8fda33f47d700d17c1a9415))

## [0.70.1](https://gitlab.com/getanthill/datastore/compare/v0.70.0...v0.70.1) (2023-08-24)


### Bug Fixes

* improve data processings registry generation script ([0bab4ab](https://gitlab.com/getanthill/datastore/commit/0bab4ab8df21f0f05a622ceabea9f7109bcff5fe))

# [0.70.0](https://gitlab.com/getanthill/datastore/compare/v0.69.2...v0.70.0) (2023-08-22)


### Features

* **sec:** data registry automation ([509d3c4](https://gitlab.com/getanthill/datastore/commit/509d3c4688f92a9b5d535be86d6c791ebf33def1))

## [0.69.2](https://gitlab.com/getanthill/datastore/compare/v0.69.1...v0.69.2) (2023-07-25)


### Bug Fixes

* **indexes:** supports nested indexes on levels deeper than level 2 ([1874f31](https://gitlab.com/getanthill/datastore/commit/1874f3142d69df094ae82886dbcee094238eb69d))

## [0.69.1](https://gitlab.com/getanthill/datastore/compare/v0.69.0...v0.69.1) (2023-06-21)


### Bug Fixes

* **pg:** fix multi datastore pg sync handler ([396849f](https://gitlab.com/getanthill/datastore/commit/396849f0d678545af4148a383744d2d20e8f776e))

# [0.69.0](https://gitlab.com/getanthill/datastore/compare/v0.68.0...v0.69.0) (2023-06-20)


### Features

* **sec:** allows to fine control data processing with explicit authorization ([c773fc5](https://gitlab.com/getanthill/datastore/commit/c773fc57d056cfc8128ebd9f4d1e4367d6c9988f))

# [0.68.0](https://gitlab.com/getanthill/datastore/compare/v0.67.0...v0.68.0) (2023-06-01)


### Features

* supports full events validation deactivation ([69bd0eb](https://gitlab.com/getanthill/datastore/commit/69bd0eb442525fff8853c411e2070ba25142002e))

# [0.67.0](https://gitlab.com/getanthill/datastore/compare/v0.66.3...v0.67.0) (2023-05-30)


### Features

* **agg:** allows to use xBy aggregation operations ([c91f94b](https://gitlab.com/getanthill/datastore/commit/c91f94b1c63b17fd3c5acb0f532180f8c27f6387))

## [0.66.3](https://gitlab.com/getanthill/datastore/compare/v0.66.2...v0.66.3) (2023-05-17)


### Bug Fixes

* **api:** release the constraint on encrypted fields schem ([e6285e7](https://gitlab.com/getanthill/datastore/commit/e6285e7ec4fb04a1432f7c407997a8cd7c3f02f6))

## [0.66.2](https://gitlab.com/getanthill/datastore/compare/v0.66.1...v0.66.2) (2023-05-17)


### Bug Fixes

* **api:** updates OpenAPI specification to suppport new `decrypt` header ([aad5b73](https://gitlab.com/getanthill/datastore/commit/aad5b73729269594807589bc428cf8a89bc183a9))

## [0.66.1](https://gitlab.com/getanthill/datastore/compare/v0.66.0...v0.66.1) (2023-05-17)


### Bug Fixes

* **sec:** check decrypt token on find and get routes if required ([ef37f85](https://gitlab.com/getanthill/datastore/commit/ef37f85450e05981c6e8697618c3c4752166482c))

# [0.66.0](https://gitlab.com/getanthill/datastore/compare/v0.65.0...v0.66.0) (2023-05-17)


### Features

* **cli:** allows to define headers in runner command ([17c8a02](https://gitlab.com/getanthill/datastore/commit/17c8a0291030ec640100ff9f2de63a5872136146))

# [0.65.0](https://gitlab.com/getanthill/datastore/compare/v0.64.1...v0.65.0) (2023-05-17)


### Features

* **api:** supports decrypt in find and get entities API routes ([fc70316](https://gitlab.com/getanthill/datastore/commit/fc703163911f2b7f687358beab2120f48a557cc9))

## [0.64.1](https://gitlab.com/getanthill/datastore/compare/v0.64.0...v0.64.1) (2023-05-10)


### Bug Fixes

* **cli:** fix skip validation field in find ([6d99259](https://gitlab.com/getanthill/datastore/commit/6d9925963c7fe7a969c6ef36bc3dc25269d02aca))

# [0.64.0](https://gitlab.com/getanthill/datastore/compare/v0.63.11...v0.64.0) (2023-05-04)


### Features

* supports namespace in Metabase data model synchronization ([d456389](https://gitlab.com/getanthill/datastore/commit/d4563892b66fcaeda6a98539c4af2898fb930be9))

## [0.63.11](https://gitlab.com/getanthill/datastore/compare/v0.63.10...v0.63.11) (2023-05-04)


### Bug Fixes

* **pg:** supports defining namespace in pg sync from handler URL ([5d0affd](https://gitlab.com/getanthill/datastore/commit/5d0affd788ccd967864eced632f8bf63de21c941))

## [0.63.10](https://gitlab.com/getanthill/datastore/compare/v0.63.9...v0.63.10) (2023-05-02)


### Bug Fixes

* remove sensitive env variables in Dockerfile ([4279f6a](https://gitlab.com/getanthill/datastore/commit/4279f6a61ce45e53e7a9b293fe1a26c849570467))

## [0.63.9](https://gitlab.com/getanthill/datastore/compare/v0.63.8...v0.63.9) (2023-05-02)


### Bug Fixes

* **sonar:** remove code smells ([21afb05](https://gitlab.com/getanthill/datastore/commit/21afb05df4bc8bf31ed6c17a3bc31f96562c3a26))

## [0.63.8](https://gitlab.com/getanthill/datastore/compare/v0.63.7...v0.63.8) (2023-05-02)


### Bug Fixes

* await amqp closing ([b17f754](https://gitlab.com/getanthill/datastore/commit/b17f754bdde7ebe73fd094e92b86f02adae66cc2))
* **dep:** bump event-source dependency ([abc7962](https://gitlab.com/getanthill/datastore/commit/abc7962765faa23d51300a24e9912f04a13365ca))
* **sonar:** improve reliability ([0d7f088](https://gitlab.com/getanthill/datastore/commit/0d7f0881c7688dc921fd2d32c26be2610bb9cac0))

## [0.63.7](https://gitlab.com/getanthill/datastore/compare/v0.63.6...v0.63.7) (2023-05-02)


### Bug Fixes

* **sdk:** fix walk multi models with same sort condition values ([6b60547](https://gitlab.com/getanthill/datastore/commit/6b6054797416f733a1709972d078b15c8aaeba8d))

## [0.63.6](https://gitlab.com/getanthill/datastore/compare/v0.63.5...v0.63.6) (2023-04-27)


### Bug Fixes

* **pg:** supports namespace in PostgreSQL data synchronization ([2a8baf5](https://gitlab.com/getanthill/datastore/commit/2a8baf595fa4b49b5f9fc44473864e860cd77ad1))

## [0.63.5](https://gitlab.com/getanthill/datastore/compare/v0.63.4...v0.63.5) (2023-04-26)


### Bug Fixes

* **ci:** exclude steps on commit message ([3e5b10b](https://gitlab.com/getanthill/datastore/commit/3e5b10b79ff42a3f36c343ea2f6527995b2f8f16))

## [0.63.4](https://gitlab.com/getanthill/datastore/compare/v0.63.3...v0.63.4) (2023-04-24)


### Bug Fixes

* **dep:** bump package ([b5e6301](https://gitlab.com/getanthill/datastore/commit/b5e630119ec5671c9656c4d1c92a60a9d3f5112f))

## [0.63.3](https://gitlab.com/getanthill/datastore/compare/v0.63.2...v0.63.3) (2023-04-24)


### Bug Fixes

* bump dependencies ([77c8214](https://gitlab.com/getanthill/datastore/commit/77c8214cfa6c83c81c65ba0e523f450d1950ceb2))

## [0.63.2](https://gitlab.com/getanthill/datastore/compare/v0.63.1...v0.63.2) (2023-04-17)


### Bug Fixes

* **cli:** moves the update many logic into the models domain ([c8edb42](https://gitlab.com/getanthill/datastore/commit/c8edb42e1a76964a6ec0d19b123e699ea3414bff))

## [0.63.1](https://gitlab.com/getanthill/datastore/compare/v0.63.0...v0.63.1) (2023-04-13)


### Bug Fixes

* **cli:** supports complex args in stdin ([a3f4b34](https://gitlab.com/getanthill/datastore/commit/a3f4b3484153becdf8150339fe1d5a1537a2a2cc))

# [0.63.0](https://gitlab.com/getanthill/datastore/compare/v0.62.0...v0.63.0) (2023-04-13)


### Bug Fixes

* **cli:** remove extra log on history creation ([3975614](https://gitlab.com/getanthill/datastore/commit/3975614c7eeec092446cbf255c311f90a5467d06))


### Features

* **cli:** improve cli user experience ([8b12b29](https://gitlab.com/getanthill/datastore/commit/8b12b292513af57fc3b457d4e77825f9f64556e5))

# [0.62.0](https://gitlab.com/getanthill/datastore/compare/v0.61.1...v0.62.0) (2023-04-13)


### Features

* **cli:** cli improvement ([8e68fe1](https://gitlab.com/getanthill/datastore/commit/8e68fe171ed97cc2ec51539ab821a714a24fc5e6))

## [0.61.1](https://gitlab.com/getanthill/datastore/compare/v0.61.0...v0.61.1) (2023-04-13)


### Bug Fixes

* **cli:** resets default command values ([41f6651](https://gitlab.com/getanthill/datastore/commit/41f6651665f4e04560bdbda8ca64e306b0dc514c))

# [0.61.0](https://gitlab.com/getanthill/datastore/compare/v0.60.9...v0.61.0) (2023-04-12)


### Features

* **cli:** supports infinite cli commands ([20f089a](https://gitlab.com/getanthill/datastore/commit/20f089afd1ce4f0d06284bf3466d00f275ea9105))

## [0.60.9](https://gitlab.com/getanthill/datastore/compare/v0.60.8...v0.60.9) (2023-04-12)


### Bug Fixes

* use normalized field naming in Metabase model sync ([bfe9fdb](https://gitlab.com/getanthill/datastore/commit/bfe9fdbad5b71325e850208be4ab57ccce86c540))

## [0.60.8](https://gitlab.com/getanthill/datastore/compare/v0.60.7...v0.60.8) (2023-04-11)


### Bug Fixes

* **metabase:** keeps previous field visibility if not controlled yet ([53bb731](https://gitlab.com/getanthill/datastore/commit/53bb731278b4be1a2b3ba3a00f1822d9d32d4edd))

## [0.60.7](https://gitlab.com/getanthill/datastore/compare/v0.60.6...v0.60.7) (2023-04-11)


### Bug Fixes

* **metabase:** improves support on PostgreSQL data model sync ([3cd49bb](https://gitlab.com/getanthill/datastore/commit/3cd49bb8dfdd61f22485c48c5d1b9662bf488844))

## [0.60.6](https://gitlab.com/getanthill/datastore/compare/v0.60.5...v0.60.6) (2023-04-11)


### Bug Fixes

* **pg:** allows to update entity or event ([2fd2be6](https://gitlab.com/getanthill/datastore/commit/2fd2be6398e07b26331e4e04b73e4adf30f15eca))

## [0.60.5](https://gitlab.com/getanthill/datastore/compare/v0.60.4...v0.60.5) (2023-04-11)


### Bug Fixes

* **metabase:** fix fields update in model synchronization script ([c4776aa](https://gitlab.com/getanthill/datastore/commit/c4776aa194f9b72b186d4aa2bd776b57d29c9904))
* **pg:** supports upsert on data sync in PostgreSQL ([633956b](https://gitlab.com/getanthill/datastore/commit/633956b86cf5ef76281ad5c48b28b126b50aab07))

## [0.60.4](https://gitlab.com/getanthill/datastore/compare/v0.60.3...v0.60.4) (2023-04-11)


### Bug Fixes

* fix error print in runner initialization ([344447f](https://gitlab.com/getanthill/datastore/commit/344447ff398870b2ff5ae304abe544193ccf47c8))

## [0.60.3](https://gitlab.com/getanthill/datastore/compare/v0.60.2...v0.60.3) (2023-04-11)


### Bug Fixes

* **walk:** sets exhausted queries with max version -1 ([5d2567f](https://gitlab.com/getanthill/datastore/commit/5d2567fbb80dd0f11352d802a37da73994345dbc))

## [0.60.2](https://gitlab.com/getanthill/datastore/compare/v0.60.1...v0.60.2) (2023-04-06)


### Bug Fixes

* **walk:** improves walk performance some queries with versions holes ([b1a90b5](https://gitlab.com/getanthill/datastore/commit/b1a90b58c5ae9d237acd3d1faa1afd30a1fefda5))

## [0.60.1](https://gitlab.com/getanthill/datastore/compare/v0.60.0...v0.60.1) (2023-04-06)


### Bug Fixes

* **walk:** fix special use case with _fields included in query ([47c91a2](https://gitlab.com/getanthill/datastore/commit/47c91a2a7ddd64bd829e1bf21d9bb00951dc8975))

# [0.60.0](https://gitlab.com/getanthill/datastore/compare/v0.59.2...v0.60.0) (2023-04-05)


### Features

* **data:** add standard projection synchronization in pg ([95243e8](https://gitlab.com/getanthill/datastore/commit/95243e85a81b1c6a8c66f1793fb2dc394b505346))

## [0.59.2](https://gitlab.com/getanthill/datastore/compare/v0.59.1...v0.59.2) (2023-04-03)


### Bug Fixes

* embed pg service in typescript build ([3b975d0](https://gitlab.com/getanthill/datastore/commit/3b975d0e7eb520fe9e874585ba190ff691e6e662))

## [0.59.1](https://gitlab.com/getanthill/datastore/compare/v0.59.0...v0.59.1) (2023-04-03)


### Bug Fixes

* fix case where queries are exhausted but results ([281a983](https://gitlab.com/getanthill/datastore/commit/281a983a8ec3df3ee044d2a67521417781cae967))

# [0.59.0](https://gitlab.com/getanthill/datastore/compare/v0.58.0...v0.59.0) (2023-04-03)


### Features

* **sql:** supports sync data into a PostgreSQL database ([b52a771](https://gitlab.com/getanthill/datastore/commit/b52a77102d8db76517f9323c38e4fb33102cd3cd))

# [0.58.0](https://gitlab.com/getanthill/datastore/compare/v0.57.6...v0.58.0) (2023-03-30)


### Features

* script to wait for RabbitMQ being up ([e2f673d](https://gitlab.com/getanthill/datastore/commit/e2f673de46e411ce00469f355eeb538afea6fbb7))

## [0.57.6](https://gitlab.com/getanthill/datastore/compare/v0.57.5...v0.57.6) (2023-03-26)


### Bug Fixes

* fix users model to make it available by default ([4751015](https://gitlab.com/getanthill/datastore/commit/4751015206251a32fa7ce9e6305f1d9666b88677))
* **metabase:** integrates hidden fields available in table metadata ([d02735f](https://gitlab.com/getanthill/datastore/commit/d02735fafb402de5df4d5966cbd92add2c91b75e))

## [0.57.5](https://gitlab.com/getanthill/datastore/compare/v0.57.4...v0.57.5) (2023-02-15)


### Bug Fixes

* supports void headers in page and page-size ([6325684](https://gitlab.com/getanthill/datastore/commit/632568434dc3cec3e56aad007c72aa30249a7dcb))

## [0.57.4](https://gitlab.com/getanthill/datastore/compare/v0.57.3...v0.57.4) (2023-02-15)


### Bug Fixes

* improve performance setting the MongoDB cursor batchSize ([dba4cb5](https://gitlab.com/getanthill/datastore/commit/dba4cb54d1e079d29d1eb4e140c02129e763e410))
* send `page` and `page-size` 0 values ([f0fb808](https://gitlab.com/getanthill/datastore/commit/f0fb8081bed1a06887115e52e724c4c5767e0025))

## [0.57.3](https://gitlab.com/getanthill/datastore/compare/v0.57.2...v0.57.3) (2023-02-09)


### Bug Fixes

* **boot:** improve boot time optimizing opentelemetry instrumentation ([e5dcf51](https://gitlab.com/getanthill/datastore/commit/e5dcf5150b6d49357fcd24341b29035ef3562751))

## [0.57.2](https://gitlab.com/getanthill/datastore/compare/v0.57.1...v0.57.2) (2023-02-09)


### Bug Fixes

* bump telemetry ([44679a0](https://gitlab.com/getanthill/datastore/commit/44679a089001c8c3719f60213df36274b0949b3b))

## [0.57.1](https://gitlab.com/getanthill/datastore/compare/v0.57.0...v0.57.1) (2023-02-08)


### Bug Fixes

* **metrics:** improves metrics memory footprint ([94a1d95](https://gitlab.com/getanthill/datastore/commit/94a1d9593e990d41ba601f479d00b28c973bd53e))

# [0.57.0](https://gitlab.com/getanthill/datastore/compare/v0.56.5...v0.57.0) (2023-02-07)


### Features

* **tel:** improve telemetry metrics ([14ac211](https://gitlab.com/getanthill/datastore/commit/14ac211f324f868f554401cccf02df99979f1e56))

## [0.56.5](https://gitlab.com/getanthill/datastore/compare/v0.56.4...v0.56.5) (2023-02-03)


### Bug Fixes

* **amqp:** improve invalid or badly routed messages ([7acc655](https://gitlab.com/getanthill/datastore/commit/7acc655d1888c60bca674859bbfbee744dbb62ac))
* improve stream HTTP testing ([9c45144](https://gitlab.com/getanthill/datastore/commit/9c45144323d5beac6de343fafc694fe46e4f516f))

## [0.56.4](https://gitlab.com/getanthill/datastore/compare/v0.56.3...v0.56.4) (2023-02-02)


### Bug Fixes

* amqp redelivery ([ff67ffe](https://gitlab.com/getanthill/datastore/commit/ff67ffe024cdc36aeaad839cc8f84ae6f5a52207))

## [0.56.3](https://gitlab.com/getanthill/datastore/compare/v0.56.2...v0.56.3) (2023-01-31)


### Bug Fixes

* fix invalid number or integer deepCoerce ([eea33ef](https://gitlab.com/getanthill/datastore/commit/eea33eff753474a560cbd933e666d9a83f3b359a))

## [0.56.2](https://gitlab.com/getanthill/datastore/compare/v0.56.1...v0.56.2) (2023-01-30)


### Bug Fixes

* allows to consume multiple queues simultaneously ([8964e38](https://gitlab.com/getanthill/datastore/commit/8964e385ddeff9df617d1ffed71c207fa2f423a9))

## [0.56.1](https://gitlab.com/getanthill/datastore/compare/v0.56.0...v0.56.1) (2023-01-30)


### Bug Fixes

* optimizes AMQP consumption ([f7b47f7](https://gitlab.com/getanthill/datastore/commit/f7b47f76e6dead4684ce49e3fbd9c298e1b5ed30))

# [0.56.0](https://gitlab.com/getanthill/datastore/compare/v0.55.1...v0.56.0) (2023-01-26)


### Features

* metabase fields sync extend ([a45ca87](https://gitlab.com/getanthill/datastore/commit/a45ca87ffd6f57419a8d2c82e6df1f8771e06962))

## [0.55.1](https://gitlab.com/getanthill/datastore/compare/v0.55.0...v0.55.1) (2023-01-26)


### Bug Fixes

* force a property being present in an event to be processed ([eae1f34](https://gitlab.com/getanthill/datastore/commit/eae1f341a52b038d18efa5a6d9b15a803634c8ab))

# [0.55.0](https://gitlab.com/getanthill/datastore/compare/v0.54.2...v0.55.0) (2023-01-24)


### Features

* **sec:** allows to reencrypt some models only ([fd197ba](https://gitlab.com/getanthill/datastore/commit/fd197ba88701372878eba8fd4d55905bd5cf029b))

## [0.54.2](https://gitlab.com/getanthill/datastore/compare/v0.54.1...v0.54.2) (2023-01-16)


### Bug Fixes

* bump event-source package ([1d9e3bc](https://gitlab.com/getanthill/datastore/commit/1d9e3bca5e03075fc839c889381e932df4e3e4b1))

## [0.54.1](https://gitlab.com/getanthill/datastore/compare/v0.54.0...v0.54.1) (2023-01-16)


### Bug Fixes

* implements sse heartbeat to keep connection alive ([fc32a91](https://gitlab.com/getanthill/datastore/commit/fc32a91500dbad46f225f3cd56eec0999ab58ad8))

# [0.54.0](https://gitlab.com/getanthill/datastore/compare/v0.53.2...v0.54.0) (2023-01-16)


### Bug Fixes

* skips to start Prometheus server for sdk by default ([3e8e8c0](https://gitlab.com/getanthill/datastore/commit/3e8e8c00e69b7a11fd172a8e2f462202755e1d9b))


### Features

* allows to skip to entity state persistence in event reduction ([41b30db](https://gitlab.com/getanthill/datastore/commit/41b30db0d3a62c240524e2fd462009f3372daf40))

## [0.53.2](https://gitlab.com/getanthill/datastore/compare/v0.53.1...v0.53.2) (2023-01-16)


### Bug Fixes

* performances improvement ([cbeee61](https://gitlab.com/getanthill/datastore/commit/cbeee614672da69d8efb6e3a4c700ab783d9ce9e))

## [0.53.1](https://gitlab.com/getanthill/datastore/compare/v0.53.0...v0.53.1) (2023-01-13)


### Bug Fixes

* bump telemetry to expose Prometheus metrics ([ba1ca29](https://gitlab.com/getanthill/datastore/commit/ba1ca29765a898a05a85a71549fc125850b54564))

# [0.53.0](https://gitlab.com/getanthill/datastore/compare/v0.52.5...v0.53.0) (2023-01-13)


### Features

* upgrade opentelemetry ([e8e71c1](https://gitlab.com/getanthill/datastore/commit/e8e71c12680c7a0d34872ef497ada050fd563009))

## [0.52.5](https://gitlab.com/getanthill/datastore/compare/v0.52.4...v0.52.5) (2023-01-11)


### Bug Fixes

* **sec:** use node user in Dockerfile ([9d1a23e](https://gitlab.com/getanthill/datastore/commit/9d1a23ed848e74ba3ed2f8ab5916e4081526034f))

## [0.52.4](https://gitlab.com/getanthill/datastore/compare/v0.52.3...v0.52.4) (2023-01-10)


### Bug Fixes

* add script to wait for MongoDb connection ([c83b0ed](https://gitlab.com/getanthill/datastore/commit/c83b0ed093ad9bb304e5ad9abd3664002e22a13e))

## [0.52.3](https://gitlab.com/getanthill/datastore/compare/v0.52.2...v0.52.3) (2023-01-05)


### Bug Fixes

* **runner:** resolve immediately for HTTP stream allowing parallel streams ([cbf7fbb](https://gitlab.com/getanthill/datastore/commit/cbf7fbb99ccfe42c6b4def89c75600ae7c83fe73))

## [0.52.2](https://gitlab.com/getanthill/datastore/compare/v0.52.1...v0.52.2) (2023-01-02)


### Bug Fixes

* bump api-validators dependency to solve memory leak issue ([bc0aab3](https://gitlab.com/getanthill/datastore/commit/bc0aab31f253a90628ccb3f74adee600fdf91e0d))

## [0.52.1](https://gitlab.com/getanthill/datastore/compare/v0.52.0...v0.52.1) (2022-12-22)


### Bug Fixes

* use default typings ([ebb259f](https://gitlab.com/getanthill/datastore/commit/ebb259ff2f77b378cf708fbc029cfad0cc7f5c9b))

# [0.52.0](https://gitlab.com/getanthill/datastore/compare/v0.51.0...v0.52.0) (2022-12-22)


### Features

* encryption keys rotation ([b0d4656](https://gitlab.com/getanthill/datastore/commit/b0d4656677be628fdfadc1f99686ebc1812b6b47))

# [0.51.0](https://gitlab.com/getanthill/datastore/compare/v0.50.0...v0.51.0) (2022-12-15)


### Features

* **amqp:** automatic reconnect ([8db66aa](https://gitlab.com/getanthill/datastore/commit/8db66aa9574510304efc5af1d26b73353c442cad))

# [0.50.0](https://gitlab.com/getanthill/datastore/compare/v0.49.0...v0.50.0) (2022-12-13)


### Features

* update massively and stream AMQP with message validation ([a088e47](https://gitlab.com/getanthill/datastore/commit/a088e47a54c6ce0485794100e7b7da51aac9efc1))

# [0.49.0](https://gitlab.com/getanthill/datastore/compare/v0.48.1...v0.49.0) (2022-12-12)


### Features

* **stream:** stream over SSE in sdk by default ([17b49bd](https://gitlab.com/getanthill/datastore/commit/17b49bdd739b7821022f2d378746a9c4f795f2b6))

## [0.48.1](https://gitlab.com/getanthill/datastore/compare/v0.48.0...v0.48.1) (2022-11-30)


### Bug Fixes

* emits events for all kind of command ([ed00561](https://gitlab.com/getanthill/datastore/commit/ed0056119e05424224cb193dabfa869ea9d9ffc6))

# [0.48.0](https://gitlab.com/getanthill/datastore/compare/v0.47.5...v0.48.0) (2022-11-29)


### Features

* allows to define a maxTimeMS on mongodb queries ([6299e46](https://gitlab.com/getanthill/datastore/commit/6299e466db2f4903cb2502b6684074e593e3e1b5))

## [0.47.5](https://gitlab.com/getanthill/datastore/compare/v0.47.4...v0.47.5) (2022-11-29)


### Bug Fixes

* remove mongodb monitoring ([5476b5f](https://gitlab.com/getanthill/datastore/commit/5476b5f097554468bbdf0184d121b432add6aa23))

## [0.47.4](https://gitlab.com/getanthill/datastore/compare/v0.47.3...v0.47.4) (2022-11-25)


### Bug Fixes

* fix dockerfile ([610f8d2](https://gitlab.com/getanthill/datastore/commit/610f8d238804539db0c77cf304260b1793c911fa))
* **ts:** compile in commonjs back ([88814e4](https://gitlab.com/getanthill/datastore/commit/88814e489cce2e128ee73f87d98d4a61dc5b03ac))

## [0.47.3](https://gitlab.com/getanthill/datastore/compare/v0.47.2...v0.47.3) (2022-11-25)


### Bug Fixes

* build and teardown scripts fixed after upgrade ([ea6e3be](https://gitlab.com/getanthill/datastore/commit/ea6e3bea8c4673c3198c8a12089a6644de7194d8))
* remove typings to src reference to fix tsc build ([74abb36](https://gitlab.com/getanthill/datastore/commit/74abb36d062df372e812f1c8f5f6a5a77f7ea1cc))

## [0.47.2](https://gitlab.com/getanthill/datastore/compare/v0.47.1...v0.47.2) (2022-11-24)


### Bug Fixes

* **ts:** install required ajv dependency ([93ebce5](https://gitlab.com/getanthill/datastore/commit/93ebce50e2e2584a8a63202b3c83ee454f32a6bd))

## [0.47.1](https://gitlab.com/getanthill/datastore/compare/v0.47.0...v0.47.1) (2022-11-24)


### Bug Fixes

* **ts:** exclude src and typings from published npm package ([e949f6f](https://gitlab.com/getanthill/datastore/commit/e949f6fd569f29198d35496111a8a6a1d6facab0))

# [0.47.0](https://gitlab.com/getanthill/datastore/compare/v0.46.3...v0.47.0) (2022-11-24)


### Bug Fixes

* **dep:** move dev to prod typing package ([08517b3](https://gitlab.com/getanthill/datastore/commit/08517b3b41af8ec76632acb5d6712b3b09543c20))


### Features

* **cli:** allows to stream with AMQP ([5a4f5d6](https://gitlab.com/getanthill/datastore/commit/5a4f5d678c91c46e5ac173ba007f9b145e2a7e6d))

## [0.46.3](https://gitlab.com/getanthill/datastore/compare/v0.46.2...v0.46.3) (2022-11-24)


### Bug Fixes

* upgrade telemetry package to fix prometheus metrics ([7e5506a](https://gitlab.com/getanthill/datastore/commit/7e5506a2640f4eefe8717b5f2646e88d834e1370))

## [0.46.2](https://gitlab.com/getanthill/datastore/compare/v0.46.1...v0.46.2) (2022-11-22)


### Bug Fixes

* bump telemetry package to remove trace log bad format ([98e6c4a](https://gitlab.com/getanthill/datastore/commit/98e6c4ab30d48073b04499cc0ef6712261e89711))

## [0.46.1](https://gitlab.com/getanthill/datastore/compare/v0.46.0...v0.46.1) (2022-11-22)


### Bug Fixes

* fix Dockerfile prune command ([27b0fca](https://gitlab.com/getanthill/datastore/commit/27b0fca3ecac20cfc6212f5a3038745820e39115))
* middleware OpenAPI initilization ([b4ab1ad](https://gitlab.com/getanthill/datastore/commit/b4ab1adcf0f8d6179d5d7ab5c9e87cb960c135d7))

# [0.46.0](https://gitlab.com/getanthill/datastore/compare/v0.45.0...v0.46.0) (2022-11-21)


### Features

* exposes new event-based API with AMQP and MQTT ([d409a5d](https://gitlab.com/getanthill/datastore/commit/d409a5d54c9cc8a199cd10008e81d36914983c10))

# [0.45.0](https://gitlab.com/getanthill/datastore/compare/v0.44.0...v0.45.0) (2022-10-26)


### Features

* **metabase:** data model synchronization script with Metabase ([545bd05](https://gitlab.com/getanthill/datastore/commit/545bd05dcb63b09ac1d986a7033441358d907c25))

# [0.44.0](https://gitlab.com/getanthill/datastore/compare/v0.43.0...v0.44.0) (2022-10-17)


### Features

* bump NodeJS to 18 ([3529981](https://gitlab.com/getanthill/datastore/commit/3529981974d3986d8057a5982419e184c2cbc623))

# [0.43.0](https://gitlab.com/getanthill/datastore/compare/v0.42.2...v0.43.0) (2022-10-12)


### Features

* all events stream and replay ([de56946](https://gitlab.com/getanthill/datastore/commit/de569463dc1f86138d975a9d18f6a7798da42fb3))

## [0.42.2](https://gitlab.com/getanthill/datastore/compare/v0.42.1...v0.42.2) (2022-09-29)


### Bug Fixes

* exclude test scripts from sonarcloud coverage ([e57f927](https://gitlab.com/getanthill/datastore/commit/e57f9274b100bf54f36321d5c355af2f76873f31))

## [0.42.1](https://gitlab.com/getanthill/datastore/compare/v0.42.0...v0.42.1) (2022-09-29)


### Bug Fixes

* **admin:** model update spec ([d235072](https://gitlab.com/getanthill/datastore/commit/d2350721c1f1cf5a672c110290e2ca3498f52440))

# [0.42.0](https://gitlab.com/getanthill/datastore/compare/v0.41.1...v0.42.0) (2022-09-27)


### Features

* **spec:** api builder description in models ([64b9444](https://gitlab.com/getanthill/datastore/commit/64b9444c3c7c8f2dee3abc73b4a719d75bf64718))

## [0.41.1](https://gitlab.com/getanthill/datastore/compare/v0.41.0...v0.41.1) (2022-09-12)


### Bug Fixes

* remove validator script from bin ([1e314d6](https://gitlab.com/getanthill/datastore/commit/1e314d60c22d051a4bdac1de2b7f1d4ce6a3fc45))

# [0.41.0](https://gitlab.com/getanthill/datastore/compare/v0.40.1...v0.41.0) (2022-09-08)


### Features

* data deletion and data access features ([f1a6784](https://gitlab.com/getanthill/datastore/commit/f1a6784a3899767f5346fb45590fa24a12ad27c5))

## [0.40.1](https://gitlab.com/getanthill/datastore/compare/v0.40.0...v0.40.1) (2022-08-23)


### Bug Fixes

* bump dependencies for mongodb@4.9.0 ([f95a678](https://gitlab.com/getanthill/datastore/commit/f95a67875c45bf4d69f56bc5ae3bb0a8774a9a59))
* bump dependencies for mongodb@4.9.0 ([64a47e7](https://gitlab.com/getanthill/datastore/commit/64a47e7f682dcda3e15e7fafcb5bb12d44a5b9a2))

# [0.40.0](https://gitlab.com/getanthill/datastore/compare/v0.39.1...v0.40.0) (2022-08-22)


### Features

* implements JSON Patch step in Aggregation ([ce4d9ff](https://gitlab.com/getanthill/datastore/commit/ce4d9fff7e4079d50bf1d639a4842fcd2e6d15f6))

## [0.39.1](https://gitlab.com/getanthill/datastore/compare/v0.39.0...v0.39.1) (2022-08-16)


### Bug Fixes

* fix walk multi entities on events with exact page size results number ([2a3d5f5](https://gitlab.com/getanthill/datastore/commit/2a3d5f5b8923ee3242ba6e1baae14ccbfde18553))

# [0.39.0](https://gitlab.com/getanthill/datastore/compare/v0.38.0...v0.39.0) (2022-08-01)


### Features

* **agg:** improves filter logic to retrieve a single entity from a collection ([5fdd599](https://gitlab.com/getanthill/datastore/commit/5fdd599f9fe8a724abc7443aa2d48eff01b34233))

# [0.38.0](https://gitlab.com/getanthill/datastore/compare/v0.37.3...v0.38.0) (2022-07-28)


### Features

* supports multi entities projections, replay and streaming ([415ad01](https://gitlab.com/getanthill/datastore/commit/415ad0147962189cb3f7c2800dc7997bd742929a))

## [0.37.3](https://gitlab.com/getanthill/datastore/compare/v0.37.2...v0.37.3) (2022-07-21)


### Bug Fixes

* update MongoDB URL from environment variable ([c448a43](https://gitlab.com/getanthill/datastore/commit/c448a433d049f7a20c0c5fc5642564ca1d22298b))

## [0.37.2](https://gitlab.com/getanthill/datastore/compare/v0.37.1...v0.37.2) (2022-07-21)


### Bug Fixes

* bump version ([ffea0a7](https://gitlab.com/getanthill/datastore/commit/ffea0a7fc588d0294f7654f5e864045d74d3f992))

## [0.37.1](https://gitlab.com/getanthill/datastore/compare/v0.37.0...v0.37.1) (2022-07-21)


### Bug Fixes

* remove incompatible summary property from info OpenAPI spec ([0b7c6ef](https://gitlab.com/getanthill/datastore/commit/0b7c6efe3e551c103bcb7c42c0068a04c3e2cd2b))

# [0.37.0](https://gitlab.com/getanthill/datastore/compare/v0.36.1...v0.37.0) (2022-07-20)


### Features

* allows to define custom responses in custom events ([89d75ff](https://gitlab.com/getanthill/datastore/commit/89d75ff07fe0fa25740b05a2bbf5f47e4df092a2))

## [0.36.1](https://gitlab.com/getanthill/datastore/compare/v0.36.0...v0.36.1) (2022-07-19)


### Bug Fixes

* define response  error ([3bdd9e5](https://gitlab.com/getanthill/datastore/commit/3bdd9e547820c33e6a817ba2b4096f701f0c449c))

# [0.36.0](https://gitlab.com/getanthill/datastore/compare/v0.35.0...v0.36.0) (2022-07-19)


### Features

* allows to configure Open API info and servers ([324f73e](https://gitlab.com/getanthill/datastore/commit/324f73efa65b1eaf110da8fbd80e9cab9b0998e2))

# [0.35.0](https://gitlab.com/getanthill/datastore/compare/v0.34.0...v0.35.0) (2022-07-19)


### Bug Fixes

* fix typos in API spec responses title ([eebc40a](https://gitlab.com/getanthill/datastore/commit/eebc40a308918f477f9c850a5f49e3347b806448))


### Features

* show custom handler in OpenAPI specification ([6766f1b](https://gitlab.com/getanthill/datastore/commit/6766f1bc3ac06401104d80d68c72642904aec3cd))

# [0.34.0](https://gitlab.com/getanthill/datastore/compare/v0.33.2...v0.34.0) (2022-07-19)


### Features

* support custom events with encrypted fields ([0339665](https://gitlab.com/getanthill/datastore/commit/03396652233916c3e05f87f0672c90e8f461cb36))

## [0.33.2](https://gitlab.com/getanthill/datastore/compare/v0.33.1...v0.33.2) (2022-07-19)


### Bug Fixes

* update events schema ([9018b58](https://gitlab.com/getanthill/datastore/commit/9018b58c74ec1e18de18ff82dc7d9aa4a849e2e7))

## [0.33.1](https://gitlab.com/getanthill/datastore/compare/v0.33.0...v0.33.1) (2022-07-18)


### Bug Fixes

* **sdk:** align event type version to string ([5edde2a](https://gitlab.com/getanthill/datastore/commit/5edde2aa28e2e28341d8f9fabe613431972e153a))

# [0.33.0](https://gitlab.com/getanthill/datastore/compare/v0.32.0...v0.33.0) (2022-07-18)


### Features

* **cli:** extract all models available in a given instance ([232db01](https://gitlab.com/getanthill/datastore/commit/232db01a7f907cb9b0b62cff8b821b2b6a724883))

# [0.32.0](https://gitlab.com/getanthill/datastore/compare/v0.31.0...v0.32.0) (2022-07-13)


### Features

* improve models indexes creation logs ([1b2666b](https://gitlab.com/getanthill/datastore/commit/1b2666b28c167a8afad9c7c862a56bb0d693000b))

# [0.31.0](https://gitlab.com/getanthill/datastore/compare/v0.30.0...v0.31.0) (2022-07-13)


### Features

* allows to configure CORS precisely ([e23280a](https://gitlab.com/getanthill/datastore/commit/e23280acf1ef990f89c73e6ef8f689eda1c7c41d))

# [0.30.0](https://gitlab.com/getanthill/datastore/compare/v0.29.0...v0.30.0) (2022-07-12)


### Features

* improve indexes creation ([410100d](https://gitlab.com/getanthill/datastore/commit/410100dcf0bede2c5be6584d166377a3033cfaa9))

# [0.29.0](https://gitlab.com/getanthill/datastore/compare/v0.28.2...v0.29.0) (2022-07-12)


### Bug Fixes

* fix typo in environment variable naming ([dcfa00f](https://gitlab.com/getanthill/datastore/commit/dcfa00f439b6ef5ac8b725114b1682bfd41c6fb6))


### Features

* **cli:** allows to get count on events for a given model ([9a58796](https://gitlab.com/getanthill/datastore/commit/9a58796f54131316ca568ccaa1594f93f00add09))

## [0.28.2](https://gitlab.com/getanthill/datastore/compare/v0.28.1...v0.28.2) (2022-07-11)


### Bug Fixes

* remove the require of util lib in Datastore ([f65b57f](https://gitlab.com/getanthill/datastore/commit/f65b57fd535b8d619d2808ac76e62d78c851fe5e))

## [0.28.1](https://gitlab.com/getanthill/datastore/compare/v0.28.0...v0.28.1) (2022-07-08)


### Bug Fixes

* add a details object in every error response ([46e5d9c](https://gitlab.com/getanthill/datastore/commit/46e5d9c0b2109f17d57cd20752b6e18270200260))
* fix stream cli with already parsed entity ([6f8b03c](https://gitlab.com/getanthill/datastore/commit/6f8b03c5bbfa43878447f71d48fcf063c7066d1d))
* fix walk logic in case of no result on first iteration for events source ([6a53a5a](https://gitlab.com/getanthill/datastore/commit/6a53a5a13778f5f47d22a6f6bf2fb3607d1f0329))
* reduce log verbosity ([7b77e31](https://gitlab.com/getanthill/datastore/commit/7b77e31013abbb961ca46ef17c8867be9d08203e))

# [0.28.0](https://gitlab.com/getanthill/datastore/compare/v0.27.0...v0.28.0) (2022-07-04)


### Features

* allows to define expires_by value to cache entry for TTL ([93db143](https://gitlab.com/getanthill/datastore/commit/93db1432b2713c867dfc591e7941cb46a1225dd3))
* **cli:** restores an entity on a given version ([d5fe5f4](https://gitlab.com/getanthill/datastore/commit/d5fe5f418e7d6a92b86ade009180a09988d35554))

# [0.27.0](https://gitlab.com/getanthill/datastore/compare/v0.26.0...v0.27.0) (2022-06-22)


### Features

* logs a warning message on slow queries ([8da97db](https://gitlab.com/getanthill/datastore/commit/8da97db015d91ddd97cae8f136b219be1723e7ed))

# [0.26.0](https://gitlab.com/getanthill/datastore/compare/v0.25.0...v0.26.0) (2022-06-21)


### Features

* logs query planner on demand to monitor indexes performances ([234dae6](https://gitlab.com/getanthill/datastore/commit/234dae63e4e2e89e2fd32f407cd2eb9f83b8723e))

# [0.25.0](https://gitlab.com/getanthill/datastore/compare/v0.24.3...v0.25.0) (2022-06-20)


### Features

* implements Server Sent Events API ([8181a72](https://gitlab.com/getanthill/datastore/commit/8181a72ae30de0628fd8096c6e390b32bd021216))

## [0.24.3](https://gitlab.com/getanthill/datastore/compare/v0.24.2...v0.24.3) (2022-06-17)


### Bug Fixes

* performance issue identified and fixed in OpenAPI validation logic ([c19b384](https://gitlab.com/getanthill/datastore/commit/c19b384b825fd3f814de6caf72f1d09710b955c7))

## [0.24.2](https://gitlab.com/getanthill/datastore/compare/v0.24.1...v0.24.2) (2022-06-17)


### Bug Fixes

* delay the monitoring logic during application startup ([26cb9d6](https://gitlab.com/getanthill/datastore/commit/26cb9d6a8482cb6772c67091f51ee339ddec749a))

## [0.24.1](https://gitlab.com/getanthill/datastore/compare/v0.24.0...v0.24.1) (2022-06-16)


### Bug Fixes

* monitoring exit fix on startup ([109858a](https://gitlab.com/getanthill/datastore/commit/109858a05153f57884b0b99173ad3944fc5bab97))

# [0.24.0](https://gitlab.com/getanthill/datastore/compare/v0.23.2...v0.24.0) (2022-06-16)


### Features

* stops the service after a few seconds if mongodb is still unhealthy ([0208a26](https://gitlab.com/getanthill/datastore/commit/0208a263d1c964e499176af38761baa9b019c320))

## [0.23.2](https://gitlab.com/getanthill/datastore/compare/v0.23.1...v0.23.2) (2022-06-10)


### Bug Fixes

* **telemetry:** bump package to have prometheus exporter available ([fef722d](https://gitlab.com/getanthill/datastore/commit/fef722d2e90b28cf91edc2bb45b01effc06fd939))

# [0.23.0](https://gitlab.com/getanthill/datastore/compare/v0.22.0...v0.23.0) (2022-06-09)


### Features

* cache implementation for models OpenAPI definition availability on boot time ([d1bddf5](https://gitlab.com/getanthill/datastore/commit/d1bddf5559f8ff50a96f2c57e9ff5e8d61f601c0))

# [0.22.0](https://gitlab.com/getanthill/datastore/compare/v0.21.3...v0.22.0) (2022-06-08)


### Features

* improve boot time and load / reload models logic ([f2d90fc](https://gitlab.com/getanthill/datastore/commit/f2d90fcd9b027230b0d52dcecddd4e0658089ac4))
* loads some models only from configuration ([2f2bbdf](https://gitlab.com/getanthill/datastore/commit/2f2bbdfa2ad2b3cd0af12208927a96bc332eba98))
* makes the monitoring working with a single MongoDb node ([35d7c10](https://gitlab.com/getanthill/datastore/commit/35d7c106d012e21cda75352bbd936e76a4074a7f))
* skips internal models indexes initialization on startup ([77be0bf](https://gitlab.com/getanthill/datastore/commit/77be0bf8086012e2aa0b3a6ee7601c95e3ab5a18))

## [0.21.3](https://gitlab.com/getanthill/datastore/compare/v0.21.2...v0.21.3) (2022-06-06)


### Bug Fixes

* **alive:** enforce the aliveness state from config after an effective deployment ([a941535](https://gitlab.com/getanthill/datastore/commit/a9415358fc5f3f137ee0c23742e32c529f5ab671))

## [0.21.2](https://gitlab.com/getanthill/datastore/compare/v0.21.1...v0.21.2) (2022-06-06)


### Bug Fixes

* **runner:** add log to runner on start and on replay ([1458363](https://gitlab.com/getanthill/datastore/commit/14583633a1daba86de38c78ca45b47bec65ab14e))

## [0.21.1](https://gitlab.com/getanthill/datastore/compare/v0.21.0...v0.21.1) (2022-06-06)


### Bug Fixes

* allows enforcing cli query over configuration one in runner ([11b5f03](https://gitlab.com/getanthill/datastore/commit/11b5f03fae3cfefe58187949e9983044161a297f))

# [0.21.0](https://gitlab.com/getanthill/datastore/compare/v0.20.2...v0.21.0) (2022-06-02)


### Features

* replay with event order ([dc3063d](https://gitlab.com/getanthill/datastore/commit/dc3063db40ebbd7f4d07c72637d22ac36c25c1f5))

## [0.20.1](https://gitlab.com/getanthill/datastore/compare/v0.20.0...v0.20.1) (2022-05-22)


### Bug Fixes

* **cli:** use the CLI without ds connection available ([09417a4](https://gitlab.com/getanthill/datastore/commit/09417a4a524a4b0aae8d4fcce4e70ec0c2308677))

# [0.20.0](https://gitlab.com/getanthill/datastore/compare/v0.19.0...v0.20.0) (2022-05-21)


### Features

* **cli:** supports communicating with several ds from cli ([0090ac0](https://gitlab.com/getanthill/datastore/commit/0090ac0b8b634fbdc82d0e04dcfdc9861f41f51e))

# [0.19.0](https://gitlab.com/getanthill/datastore/compare/v0.18.0...v0.19.0) (2022-05-20)


### Features

* supports global entities versioning and blockchain logic ([3bc8c63](https://gitlab.com/getanthill/datastore/commit/3bc8c63dece8477aef5fe94461d02a7fc744e428))

# [0.18.0](https://gitlab.com/getanthill/datastore/compare/v0.17.0...v0.18.0) (2022-05-19)


### Features

* **dep:** bump event-source to start supporting blockchain logic ([3ee19ad](https://gitlab.com/getanthill/datastore/commit/3ee19ade3bbcc1d7766fe3c00b5861e5162c6dc6))

# [0.17.0](https://gitlab.com/getanthill/datastore/compare/v0.16.0...v0.17.0) (2022-05-19)


### Features

* **agg:** allows to perform timetravel fetch in aggregations ([5aecd95](https://gitlab.com/getanthill/datastore/commit/5aecd95fb5ce951f11057f7070b3efb804b34b81))

# [0.16.0](https://gitlab.com/getanthill/datastore/compare/v0.15.0...v0.16.0) (2022-05-19)


### Features

* invert the readiness and aliveness probes ([ebf25dc](https://gitlab.com/getanthill/datastore/commit/ebf25dcd33d654cefd88db8b17cd3b7b27843a2a))

# [0.15.0](https://gitlab.com/getanthill/datastore/compare/v0.14.1...v0.15.0) (2022-05-18)


### Bug Fixes

* earn one iteration on walk ([757363a](https://gitlab.com/getanthill/datastore/commit/757363a6fcd3384eafae3a8778402b720cf63464))


### Features

* **cli:** adds a command to perform a data validation on a collection ([1ea81ac](https://gitlab.com/getanthill/datastore/commit/1ea81ac58aadf4364991b43887a5a2422b51c2af))

## [0.14.1](https://gitlab.com/getanthill/datastore/compare/v0.14.0...v0.14.1) (2022-05-18)


### Bug Fixes

* **typing:** fix importsNotUsedAsValues typing error in graph ([5102f12](https://gitlab.com/getanthill/datastore/commit/5102f1258223d06cb39cb18ffb65f4b63c9d3b19))

# [0.14.0](https://gitlab.com/getanthill/datastore/compare/v0.13.2...v0.14.0) (2022-05-16)


### Features

* graph generation, readonly, archive, delete ([0d96af2](https://gitlab.com/getanthill/datastore/commit/0d96af21ad4773adbc0634b00672918a24e78f34))

## [0.13.2](https://gitlab.com/getanthill/datastore/compare/v0.13.1...v0.13.2) (2022-05-16)


### Bug Fixes

* adds a command to validate a projection ([9f1d25e](https://gitlab.com/getanthill/datastore/commit/9f1d25ed8fde1b33a76e2dfdf485f4867eeb69a6))

## [0.13.1](https://gitlab.com/getanthill/datastore/compare/v0.13.0...v0.13.1) (2022-05-11)


### Bug Fixes

* increase default value for `MONGO_CONNECT_TIMEOUT_IN_MILLISECONDS` ([dcfb621](https://gitlab.com/getanthill/datastore/commit/dcfb6218aa819db6153c70cca4417a664207cb97))

# [0.13.0](https://gitlab.com/getanthill/datastore/compare/v0.12.4...v0.13.0) (2022-05-11)


### Features

* allows to test projections ([5f75567](https://gitlab.com/getanthill/datastore/commit/5f75567e7fb5886dbfda74318a28237ca30f23e5))

## [0.12.4](https://gitlab.com/getanthill/datastore/compare/v0.12.3...v0.12.4) (2022-04-30)


### Bug Fixes

* imperative on upsert ([ffdf8c7](https://gitlab.com/getanthill/datastore/commit/ffdf8c71a6abbcf05312ab3e1d253320a7473e90))

## [0.12.3](https://gitlab.com/getanthill/datastore/compare/v0.12.2...v0.12.3) (2022-04-15)


### Bug Fixes

* supports boolean array find query in deepCoerce ([de34da6](https://gitlab.com/getanthill/datastore/commit/de34da6744e8bca4611a06eadefc195c62f79a27))

## [0.12.2](https://gitlab.com/getanthill/datastore/compare/v0.12.1...v0.12.2) (2022-04-12)


### Bug Fixes

* apply query params types coercion in find controller ([ea96c80](https://gitlab.com/getanthill/datastore/commit/ea96c80b88084d8026e224ff1d617344a768329d))

## [0.12.1](https://gitlab.com/getanthill/datastore/compare/v0.12.0...v0.12.1) (2022-04-11)


### Bug Fixes

* allows to find items with correlation IDs composed of digits only ([2acd24d](https://gitlab.com/getanthill/datastore/commit/2acd24df6a5cb27405c1a72616a486e3cbba1ed5))

# [0.12.0](https://gitlab.com/getanthill/datastore/compare/v0.11.1...v0.12.0) (2022-04-08)


### Features

* **timetravel:** allows to access the state of an entity on a specific date ([a0ac88d](https://gitlab.com/getanthill/datastore/commit/a0ac88d7bfffec7dd9f1f7e7d6c7a2a3f1ab06cb))

## [0.11.1](https://gitlab.com/getanthill/datastore/compare/v0.11.0...v0.11.1) (2022-04-05)


### Bug Fixes

* bump version ([419ac40](https://gitlab.com/getanthill/datastore/commit/419ac40d6d8c3546596a7d70ccaa6d051fa716f9))
* keep dist in the artifacts ([fdd55bf](https://gitlab.com/getanthill/datastore/commit/fdd55bf00a3f92884e1f431e53aa357cc4b38b52))
* propagate dist directory between steps ([7fc32d0](https://gitlab.com/getanthill/datastore/commit/7fc32d08f2166b000d34718b839e3a95701757c2))
* publish on package registry in addition of npm ([349217d](https://gitlab.com/getanthill/datastore/commit/349217d76f74b69b8d5f693ab66d63776e552741))

# [0.11.0](https://gitlab.com/getanthill/datastore/compare/v0.10.13...v0.11.0) (2022-04-01)


### Features

* update users template ([4b51753](https://gitlab.com/getanthill/datastore/commit/4b5175395cac8b45f5f1c97a524545b25bb26350))

## [0.10.13](https://gitlab.com/getanthill/datastore/compare/v0.10.12...v0.10.13) (2022-03-31)


### Bug Fixes

* fix docker login step ([3b95329](https://gitlab.com/getanthill/datastore/commit/3b9532912ce855d5f4fc2f5469c4e8d401e0af3e))

## [0.10.12](https://gitlab.com/getanthill/datastore/compare/v0.10.11...v0.10.12) (2022-03-31)


### Bug Fixes

* bump version ([fcf871a](https://gitlab.com/getanthill/datastore/commit/fcf871aa60652b7f1c659e1c7814122829277012))
* configure semantic release ([d6c577d](https://gitlab.com/getanthill/datastore/commit/d6c577d573297003ed654b4f8e7bc17cd9d112d9))
* split docker build from semantic release ([9acca6d](https://gitlab.com/getanthill/datastore/commit/9acca6d4fe0aad8e5ca97cd06017f6a22505eb25))
