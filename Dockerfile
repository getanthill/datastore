FROM node:20 AS base

# Create app directory
WORKDIR /app

COPY .npmrc package*.json ./
COPY tsconfig.json tsconfig.sdk.json ./

# ---- Dependencies ----
FROM base AS dependencies
ARG BUILD_ENV=production

COPY --from=base /app/ .

RUN npm ci --force --ignore-scripts

# ---- Copy Files/Build ----
FROM dependencies AS build
WORKDIR /app

COPY --from=base /app/ .

COPY src src

RUN npm run build
RUN npm prune --production --force

# ---- Copy Files/Build ----
FROM node:20 AS test
WORKDIR /app

RUN apt update && apt install libcurl4 -y --no-install-recommends

COPY --from=build /app/ .
COPY --from=dependencies /app/node_modules node_modules

COPY jest.config.js jest.bench.config.js jest-mongodb-config.js .babelrc .prettierrc.json ./
COPY test test
COPY scripts scripts

ENV MONGOMS_DOWNLOAD_URL=http://downloads.mongodb.org/linux/mongodb-linux-x86_64-debian10-4.4.15.tgz

CMD ["npm", "test"]

# --- Release with Slim ----
FROM node:20-slim AS release
WORKDIR /app

COPY --from=base /app/package.json .
COPY --from=build /app/dist dist
COPY --from=build /app/node_modules node_modules
COPY scripts scripts

ARG NODE_ENV=production

EXPOSE 3001/tcp
EXPOSE 9464/tcp

USER node

CMD ["node", "--no-warnings", "dist/server"]
