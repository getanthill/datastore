# `getanthill` Datastore

[![pipeline](https://gitlab.com/getanthill/datastore/badges/master/pipeline.svg)](https://gitlab.com/getanthill/datastore/-/commits/master) [![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fdatastore&metric=alert_status)](https://sonarcloud.io/dashboard?id=getanthill%2Fdatastore)

[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fdatastore&metric=coverage)](https://sonarcloud.io/dashboard?id=getanthill%2Fdatastore) [![Security Rating](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fdatastore&metric=security_rating)](https://sonarcloud.io/dashboard?id=getanthill%2Fdatastore) [![Reliability Rating](https://sonarcloud.io/api/project_badges/measure?project=getanthill%2Fdatastore&metric=reliability_rating)](https://sonarcloud.io/dashboard?id=getanthill%2Fdatastore)

[![Twitter](https://img.shields.io/twitter/url/https/twitter.com/getanthill.svg?style=social&label=Follow%20%40getanthill)](https://twitter.com/getanthill)

## Purpose

The goal of this project is to provide a system to easily access the full power of Event-Source / CQRS systems.

## Documentation

> [https://datastore.getanthill.org/](https://datastore.getanthill.org)

## Main Features

### Manage all your Data in an Event-Source manner

Manage every single entity of your Data as a simple Event-Source entity. Track atomic updates. Restores entities at a given version. Timetravel to know what was the exact state of an entity on a given date...

### Contractualize 100% of your Data Model

Every Data in the `Datastore` is contractualized thanks to the `json-schema` [^1] standard. This standard is allowing you to access a strict documentation in your RESTful API with OpenAPI 3.0 (ex-Swagger) [^2] or in your streamed events.

### Use `streams` to process your Data in realtime

Stream your data with ease thanks to the `stream` entrypoint of the `Datastore`'s API. You can use it to deploy workers easily with automatic reconnection, pattern matching and logging.

### Access an explicit and compliant JSON Schema / OpenAPI 3.0 documentation of your Data

100% of the Data handled by the `Datastore` is available through an compliant OpenAPI 3.0 [^2] specification. You can add as many event or business rule you need in your system and make it available to everyone.

### Encrypt fields in your Data with ease

Encrypt fields of your Data to make it inaccessible by users having access to your database. You can perform multiple keys encryption, keys rotation, on-demand document encryptionm etc.

### Manage access roles between `READ`, `DECRYPT`, `WRITE` and `ADMIN`

Access to the Data is controlled with 4 different levels.

- `READ` tokens can only read Data, potentially encrypted Data.
- `DECRYPT` tokens can read Data clearly
- `WRITE` tokens can write Data in the `Datastore`
- `ADMIN` tokens can create new models and indexes

### Perform smart aggregations to chain projections between your

`Datastores`

In addition of the `sdk` available to communicate with the `Datastore` API easily, an `aggregation` pipeline is made availabe to perform complex aggregations between different instances of the `Datastore` to handle `projections`, trigger specific branches of a business logic or keep track of some events.
