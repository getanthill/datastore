const http = require('http');

async function main() {
  const requestListener = function (_req, res) {
    res.writeHead(200, { 'Content-Type': 'application/json' });
    res.end('{"is_ready":true}');
  };

  const server = http.createServer(requestListener);

  server.listen(3001);
  console.log('listening on port 3001');
}

main().catch(console.error);
