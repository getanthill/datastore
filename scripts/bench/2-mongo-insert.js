const http = require('http');

const { MongoClient } = require('mongodb');

async function main() {
  const client = new MongoClient(
    process.env.MONGO_URL || 'mongodb://localhost:27017/test',
  );

  client.connect();

  const requestListener = function (req, res) {
    let body = '';

    req.on('data', function (data) {
      body += data;

      // Too much POST data, kill the connection!
      // 1e6 === 1 * Math.pow(10, 6) === 1 * 1000000 ~~~ 1MB
      if (body.length > 1e6) req.connection.destroy();
    });

    req.on('end', async function () {
      const post = JSON.parse(body);

      await client.db().collection('test').insertOne(post);

      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(post));
    });
  };

  const server = http.createServer(requestListener);

  server.listen(3001);
  console.log('listening on port 3001');
}

main().catch(console.error);
