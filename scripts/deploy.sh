#!/usr/bin/env bash
set -e

project="datastore"
gcp_project="tooling-274306"
tag_prefix="eu.gcr.io/${gcp_project}/${project}"

echo -e "[STAGING] Deploying the project: ${project}"

echo -e "> Building Docker image"
docker build --build-arg NPM_TOKEN=${NPM_TOKEN} -f Dockerfile . --tag ${project}

echo -e "> Tagging Docker image for GCP Image Registry"
docker tag ${project} ${tag_prefix}/${project}

echo -e "> Pushing the image to GCP Image Registry"
docker push eu.gcr.io/${gcp_project}/${project}

echo -e "> Deploying the new version"
cmd="gcloud run deploy staging-${project} \
  --image=${tag_prefix}:latest \
  --platform=managed \
  --region=europe-west1 \
  --project=${gcp_project} \
"

echo -e "  Deploymend gcloud: $cmd"
bash -c "${cmd}"
