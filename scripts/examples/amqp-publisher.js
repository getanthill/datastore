const { telemetry } = require('@getanthill/telemetry');

const { default: config } = require('../../dist/config');

const { default: AmqpClient } = require('../../dist/services/amqp');

async function main() {
  console.log('[amqp] Testing');

  const client = new AmqpClient(config.amqp, telemetry);

  await client.connect();

  await client.publish('topic', { hello: 'amqp' });
}

main().catch(console.error);
