const MQTT = require('async-mqtt');

const client = MQTT.connect('tcp://localhost');

const doStuff = async () => {
  console.log('Starting');
  try {
    await client.publish('wow/so/cool', `It works! ${Date.now()}`);

    // This line doesn't run until the server responds to the publish
    await client.end();
    // This line doesn't run until the client has disconnected without error
    console.log('Done');
  } catch (e) {
    // Do something about it!
    console.log(e.stack);
    process.exit();
  }
};

client.on('connect', doStuff);
