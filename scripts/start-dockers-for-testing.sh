#!/usr/bin/env bash

docker rm -f mongo6-host rabbitmq3-host mosquitto postgres15 --volumes

docker run -d --name mongo6-host -p 27017:27017 mongo:6 mongod --replSet rs

sleep 5

docker exec mongo6-host mongosh mongodb://localhost:27017 \
  --eval 'rs.initiate({"_id":"rs","members":[{"_id":0,"host":"localhost:27017"}]})'

docker run -d -p 5672:5672 -p 15672:15672 --name rabbitmq3-host rabbitmq:3-management

docker run -d --name mosquitto \
  --network host eclipse-mosquitto  mosquitto -c /mosquitto-no-auth.conf

docker run -d --name postgres15 \
  --network host \
  -e POSTGRES_PASSWORD=password \
  -e TZ=UTC \
  -e PGTZ=UTC \
  postgres:15