const { default: config } = require('../dist/config');
const { build } = require('../dist/services');
const { default: App } = require('../dist/App');

async function main() {
  const services = build({
    ...config,
    mongodb: {
      databases: [
        {
          name: 'datastore_write',
          url: 'mongodb://localhost:27017/models?authSource=admin',
          options: {
            auth: {
              username: 'datastore',
              password: 'secretPassword',
            },
            useUnifiedTopology: true,
            ssl: false,
            tlsAllowInvalidCertificates: true,
          },
        },
        {
          name: 'datastore_read',
          url: 'mongodb://localhost:27017/models?authSource=admin',
          options: {
            auth: {
              username: 'datastore',
              password: 'secretPassword',
            },
            useUnifiedTopology: true,
            ssl: false,
            tlsAllowInvalidCertificates: true,
          },
        },
      ],
    },
  });

  const app = new App(services);

  console.log('Starting...', {
    db: services.config.mongodb.databases[0],
  });
  await app.start();

  const collections = await app.services.mongodb
    .db('datastore_write')
    .listCollections()
    .toArray();
  console.log(
    'Collections',
    collections.map((c) => c.name),
  );

  console.log('Stopping...');
  await app.stop();
}

main()
  .then(() => {
    console.log('[conn] Connected successfully');
  })
  .catch((err) => {
    console.error(err);

    process.exit(1);
  });
