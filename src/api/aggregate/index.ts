import express from 'express';
import { Services } from '../../typings';

import { aggregate } from './controllers';

function routes(services: Services) {
  const app = express.Router({ mergeParams: true });

  app.post('/', aggregate(services));

  return app;
}

export default routes;
