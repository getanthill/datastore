import type { Services } from '../../typings';

import express from 'express';
import { graphqlHTTP } from 'express-graphql';
import { createGraphQLSchema } from 'openapi-to-graphql';

async function routes(services: Services, oas?: any) {
  const app = express.Router();

  const { schema } = await createGraphQLSchema(
    {
      ...oas,
      servers: [
        {
          description: 'API Server',
          url: `http://localhost:${services.config.port}/api`,
        },
      ],
    },
    {
      /**
       * @see https://github.com/IBM/openapi-to-graphql/tree/master/packages/openapi-to-graphql
       */
      operationIdFieldNames: true,
      ...services.config.graphql.openApiToGraphQL,
    },
  );

  app.use(
    graphqlHTTP({
      schema,
      graphiql: false,
      ...services.config.graphql.http,
    }),
  );

  return app;
}

export default routes;
