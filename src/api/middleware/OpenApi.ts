import type { OpenAPIV3 } from 'openapi-types';
import type { NextFunction, Request, Response } from 'express';
import type { Services, Telemetry } from '../../typings';

import { Validator } from '@getanthill/api-validators';
import express from 'express';
import cloneDeep from 'lodash/cloneDeep';

import { getEntityName } from '../spec/builder';

import { SPEC_FRAGMENT } from '../spec';

export type Builder = () => Promise<OpenAPIV3.Document>;

export interface OpenAPIConfig {
  secret: string;
  specification: OpenAPIV3.Document;
  /**
   * Not recommended to put this flag on `true`
   */
  warnOnInvalidSpecificationOnly?: boolean;
  services?: Services;
  telemetry?: Telemetry;
}

export class OpenAPIMiddleware {
  config: OpenAPIConfig;
  validator: Validator;
  builder: Builder | null;
  services: Services | undefined;

  constructor(config: OpenAPIConfig, builder: Builder | null) {
    this.config = config;
    this.validator = new Validator(SPEC_FRAGMENT as OpenAPIV3.Document);
    this.builder = builder;
    this.services = this.config.services;

    this.updateValidator(this.config.specification);
  }

  check(specification: OpenAPIV3.Document) {
    try {
      this.validator.validateSpecification(specification);
    } catch (err: any) {
      if (this.config.warnOnInvalidSpecificationOnly !== true) {
        throw err;
      }

      this.config.telemetry?.logger?.warn('[OpenAPI] Invalid specification', {
        err,
      });
    }

    return true;
  }

  updateValidator(specification: OpenAPIV3.Document) {
    this.check(specification);

    this.validator.updateSpecification(specification);
    this.validator.reset().initAjv(
      {
        useDefaults: true,
        coerceTypes: false,
        strictTypes: false,
        strict: false,
      },
      {
        useDefaults: true,
        coerceTypes: 'array',
        strictTypes: false,
        strict: false,
      },
    );

    this.validator.compile();

    return specification;
  }

  async update(specification?: any) {
    if (specification) {
      return this.updateValidator(specification);
    }

    if (typeof this.builder === 'function') {
      const _specification = await this.builder();
      return this.updateValidator(_specification);
    }
  }

  /**
   * Returns the definition and
   *
   * @param {string[]} tokens
   * @returns {callback} The middleware
   */
  public spec() {
    return (req: Request, res: Response, next: NextFunction) => {
      const definition = cloneDeep(this.validator.getSpecification());

      const filteredModels = (req.query.models || []) as any[];

      if (filteredModels.length > 0) {
        const schemas = definition.components?.schemas || {};
        const paths = definition.paths;
        const tags = definition.tags ?? [];
        definition.paths = {};
        definition.tags = [];
        definition.components = {
          ...definition.components,
          schemas: {},
        };

        for (const tag of tags) {
          /* @ts-ignore */
          if (filteredModels.includes(tag.name.toLowerCase())) {
            definition.tags.push(tag);
          }
        }

        for (const k in paths) {
          const model = k.slice(1).split('/').shift();

          if (model && filteredModels.includes(model)) {
            const entityName = getEntityName(model, true);
            definition.paths[k] = paths[k];
            definition.components.schemas![entityName] = schemas[entityName];
          }
        }
      }

      res.set('content-type', 'application/json');

      return res.send(
        Validator.replaceReferencesInSpecification(definition, ''),
      );
    };
  }

  public registerInputValidation() {
    const router = express.Router();

    // Dynamic reload the API Specification:
    if (this.builder !== null) {
      router.get(`/${this.config.secret}`, async (req, res, next) => {
        await this.update();

        next();
      });
    }

    router
      .get(`/${this.config.secret}`, this.spec())
      .use(this.validator.validateRequestMiddleware(true));

    return router;
  }

  public validateResponseMiddleware() {
    return (req: Request, res: Response, next: NextFunction) => {
      const errors = this.validator.validateResponse(req, res);
      if (errors.length) {
        const err = {
          status: 501,
          message: 'Response validation error',
          details: errors,
        };

        res.locals.meter &&
          res.locals.meter({
            state: '501',
            ...res.locals.attributes,
          });

        res.locals.tic &&
          this.services?.metrics.recordHttpRequestDuration(
            Date.now() - res.locals.tic,
            {
              status: '501',
              method: req.method,
              model: res.locals.model,
            },
          );

        return res.status(501).json(err);
      }

      res.locals.meter &&
        res.locals.meter({
          state: '200',
          ...res.locals.attributes,
        });

      res.locals.tic &&
        this.services?.metrics.recordHttpRequestDuration(
          Date.now() - res.locals.tic,
          {
            status: '200',
            method: req.method,
            model: res.locals.model,
          },
        );

      // @ts-ignore
      return res.json(res.body);
    };
  }

  public registerOutputValidation() {
    const router = express.Router();

    router.use(this.validateResponseMiddleware());

    return router;
  }
}
