import { errorHandler } from './errorHandler';

describe('middleware/errorHandler', () => {
  let logger;
  let res;

  beforeEach(() => {
    logger = {
      debug: jest.fn(),
      error: jest.fn(),
    };

    res = {
      locals: {},
      json: jest.fn(),
      status: jest.fn().mockImplementation(() => res),
    };
  });

  afterEach(() => {
    jest.restoreAllMocks();
  });

  it('returns a 500 Internal Server Error on unknown error', () => {
    const middleware = errorHandler({ telemetry: { logger } });

    const next = jest.fn();

    middleware(new Error('Ooops'), null, res, next);

    expect(next).not.toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Internal Server Error',
      status: 500,
      details: [],
    });
  });

  it('returns a 500 Internal Server Error on non object error', () => {
    const middleware = errorHandler({ telemetry: { logger } });

    const next = jest.fn();

    const err = function Error() {};
    middleware(err, null, res, next);

    expect(next).not.toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Internal Server Error',
      status: 500,
      details: [],
    });
  });

  it('returns an error with the status attached to it', () => {
    const middleware = errorHandler({ telemetry: { logger } });

    const next = jest.fn();

    const err = new Error('Ooops');
    // @ts-ignore
    err.status = 404;
    middleware(err, null, res, next);

    expect(next).not.toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Ooops',
      status: 404,
      details: [],
    });
  });

  it('hides the internal error message if internal', () => {
    const middleware = errorHandler({ telemetry: { logger } });

    const next = jest.fn();

    const err = new Error('Oops');
    // @ts-ignore
    err.status = 500;
    middleware(err, null, res, next);

    expect(next).not.toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(500);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Internal Server Error',
      status: 500,
      details: [],
    });
  });

  it('returns an error based on an object', () => {
    const middleware = errorHandler({ telemetry: { logger } });

    const next = jest.fn();

    middleware(
      {
        status: 404,
        message: 'Not Found',
      },
      null,
      res,
      next,
    );

    expect(next).not.toHaveBeenCalled();
    expect(res.status).toHaveBeenCalledWith(404);
    expect(res.json).toHaveBeenCalledWith({
      message: 'Not Found',
      status: 404,
      details: [],
    });
  });
});
