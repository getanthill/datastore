import express from 'express';
import { Services } from '../../typings';

import { authenticate, getTokensByRole } from '../middleware';

import {
  apply,
  archive,
  create,
  createSnapshot,
  encrypt,
  decrypt,
  deleteEntity,
  find,
  get,
  getEvents,
  getGraphData,
  patch,
  restore,
  timetravel,
  update,
  unarchive,
} from './controllers';

function routes(services: Services) {
  const { config } = services;
  const app = express.Router({ mergeParams: true });

  app
    .use((req, res, next) => {
      res.locals.model = req.params.model;

      next();
    })
    .get(
      '/',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      find(services),
    )
    .post(
      '/',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      create(services),
    )
    .get(
      '/events',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      getEvents(services),
    )
    .post(
      '/encrypt',
      authenticate(getTokensByRole(config.security.tokens, 'decrypt')),
      encrypt(services),
    )
    .post(
      '/decrypt',
      authenticate(getTokensByRole(config.security.tokens, 'decrypt')),
      decrypt(services),
    )
    .get(
      '/:correlation_id',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      get(services),
    )
    .post(
      '/:correlation_id',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      update(services),
    )
    .patch(
      '/:correlation_id',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      patch(services),
    )
    .get(
      '/:correlation_id/events',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      getEvents(services),
    )
    .post(
      '/:correlation_id/snapshot',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      createSnapshot(services),
    )
    .get(
      '/:correlation_id/data',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      getGraphData(services),
    )
    .post(
      '/:correlation_id/archive',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      archive(services),
    )
    .post(
      '/:correlation_id/unarchive',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      unarchive(services),
    )
    .delete(
      '/:correlation_id',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      deleteEntity(services),
    )
    .get(
      '/:correlation_id/:version',
      authenticate(getTokensByRole(config.security.tokens, 'read')),
      timetravel(services),
    )
    .post(
      '/:correlation_id/:version/restore',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      restore(services),
    )
    .post(
      '/:correlation_id/:event_type/:event_version',
      authenticate(getTokensByRole(config.security.tokens, 'write')),
      apply(services),
    );

  return app;
}

export default routes;
