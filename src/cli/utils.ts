import type { Command } from 'commander';
import type { Services } from '../typings';

import { inspect } from 'util';

import { Option } from 'commander';
import yaml from 'js-yaml';

export function log(obj: any, format = process.env.DATASTORE_CLI_FORMAT) {
  if (format === 'json') {
    console.log(JSON.stringify(obj));

    return;
  }

  if (format === 'yaml') {
    console.log(yaml.dump(obj));

    return;
  }

  console.log(inspect(obj, false, null, true));
}

export function addStandardFields(h: Command) {
  h.option('--debug', 'Debug the request', false);
  h.option('--dry-run', 'Simulate the request without executing it', false);
  h.addOption(
    new Option('--format <format>', 'Response format').choices([
      'json',
      'yaml',
    ]),
  );
}

export function addDatastoreOptions(program: Command, services: Services) {
  const dsNames = Array.from(services.datastores.keys());

  program.addOption(
    new Option('-ds, --datastore <datastore>', 'Datastore to use')
      .default(dsNames[0] || 'default')
      .choices(dsNames.length === 0 ? ['default'] : dsNames),
  );
}

export function addPaginationFields(h: Command) {
  h.option('--page <page>', 'Page', '0');
  h.option('--page-size <page-size>', 'Page size', '20');
}
