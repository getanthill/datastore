export * from './components';
export * from './events';
export * from './metrics';

export const REGEXP_DATE_ISO_STRING_8601 =
  /\d{4}-[01]\d-[0-3]\dT[0-2]\d:[0-5]\d:[0-5]\d\.\d+([+-][0-2]\d:[0-5]\d|Z)/;

export const DEFAULT_CORRELATION_FIELD = 'correlation_id';
export const DEFAULT_DATABASE_NAME = 'datastore';
export const DEFAULT_ENV_PREFIX = 'DATASTORE';
