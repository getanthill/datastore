import type { ModelConfig } from './typings';

export type { ModelConfig };

import { Datastore, Model } from './sdk';

import * as constants from './constants';

export { Datastore, Model, constants };
