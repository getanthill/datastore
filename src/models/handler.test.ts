import { handle } from './handler';

describe('models/handler', () => {
  const DEFAULT_HANDLER = `
    return [{
        ...state,
        count: state.count + event.count
      }];`;

  it('executes code dynamically within the current context', async () => {
    const res = await handle(DEFAULT_HANDLER, { count: 1 }, { count: 1 });

    expect(res).toEqual([{ count: 2 }]);
  });

  it('throws an exception in case of error', async () => {
    let error;
    try {
      await handle(DEFAULT_HANDLER, null, { count: 1 });
    } catch (err) {
      error = err;
    }

    expect(error.message.startsWith('Cannot read properties')).toEqual(true);
  });
});
