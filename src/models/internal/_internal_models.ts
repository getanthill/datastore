import type { ModelConfig } from '../../typings';

import * as c from '../../constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'internal_models';
const CORRELATION_FIELD = 'model_id';

const db = {
  ...c.COMPONENT_STRING,
  description: 'Database connection to use for this model',
};

const name = {
  ...c.COMPONENT_STRING,
  description: 'Model name',
};

const description = {
  ...c.COMPONENT_STRING,
  description: 'Model description',
};

const correlation_field = {
  ...c.COMPONENT_STRING,
  description: 'Model correlation field',
};

const retry_duration = {
  ...c.COMPONENT_NUMBER,
  description: 'Max events handler retry duration',
};

const is_enabled = c.COMPONENTS.is_enabled;

const encrypted_fields = {
  type: 'array',
  items: {
    type: 'string',
    description: 'Field path in the model of the data to be encrypted',
  },
};

const links = {
  type: 'object',
  patternProperties: {
    '^[0-9a-z_]+$': { type: 'string' },
  },
  examples: [
    {
      correlation_id: 'model_name',
    },
    {
      entity_id: 'entity_type',
    },
    {
      child_id: 'children',
      family_id: 'families',
    },
  ],
};

const indexes = {
  type: 'array',
  items: {
    type: 'object',
    properties: {
      collection: {
        type: 'string',
        description: 'Collection name for the index',
      },
      keys: {
        type: 'object',
        description: 'List of keys for the index',
      },
      opts: {
        type: 'object',
        description: 'List of options to apply during the index creation',
      },
    },
  },
};

const schema = {
  type: 'object',
  description: 'Model schema',
  properties: {
    model: {
      type: 'object',
    },
    events: {
      type: 'object',
    },
  },
};

const with_default_events = {
  type: 'boolean',
  description: 'Use default events for CRUD',
};

const with_global_version = {
  type: 'boolean',
  description: 'Use a global version index instead of a entity local one',
};

const with_blockchain_hash = {
  type: 'boolean',
  description: 'Use a blockchain logic hash generation logic',
};

const current_hash_field = {
  type: 'string',
  description:
    'Field to use for current event blockchain hash (blockchain mode activation required)',
  example: 'hash',
};

const previous_hash_field = {
  type: 'string',
  description:
    'Field to use for previous event blockchain hash (blockchain mode activation required)',
  example: 'prev',
};

const nonce_field = {
  type: 'string',
  description:
    'Field to use for blockchain nonce value (blockchain mode activation required)',
  example: 'nonce',
};

const blockchain_hash_difficulty = {
  type: 'integer',
  description: 'Number of `0` to enforce in the generated hash',
  example: 1,
};

const blockchain_hash_genesis = {
  type: 'string',
  description: 'First hash to use for the initialization of the blockchain',
  example: '0000000000000000000000000000000000000000000000000000000000000000',
};

const must_wait_state_persistence = {
  ...c.COMPONENT_BOOLEAN,
  description:
    'If `false` the event reduction does not wait the effective persistence of the entity state in the database. Unicity constraints will not be applied in this case anymore.',
};

const with_fully_homomorphic_encryption = {
  ...c.COMPONENT_BOOLEAN,
  description: 'Use Fully Homomorphic Encryption (FHE) logic',
};

const fhe_public_key_field = {
  type: 'string',
  description: 'FHE Public Key',
  example: 'public_key',
};

const processing = {
  type: 'object',
  required: [
    'field',
    'name',
    'purpose',
    'persons',
    'recipients',
    'duration_in_seconds',
  ],
  properties: {
    name: {
      type: 'string',
    },
    field: {
      type: 'string',
      description: 'Path of the field concerned by this processing.',
    },
    purpose: {
      type: 'string',
      description: 'What is the purpose of this processing.',
    },
    owner: {
      type: 'object',
      properties: {
        name: {
          type: 'string',
          example: 'John Doe',
        },
        email: c.COMPONENT_EMAIL,
        phone: c.COMPONENT_PHONE_NUMBER,
        url: c.COMPONENT_URL,
        location: c.COMPONENT_LOCATION,
      },
    },
    persons: {
      type: 'array',
      items: c.COMPONENT_STRING,
      description:
        'List of persons concerned by this processing (ie. users, employees, prospects, etc.).',
      minLength: 1,
    },
    recipients: {
      type: 'array',
      items: c.COMPONENT_STRING,
      description:
        'List of recipients involved in this processing including subcontractors.',
      minLength: 1,
    },
    tokens: {
      type: 'array',
      items: {
        type: 'string',
      },
      description:
        'List of tokens IDs authorized to access the data concerned by this processing.',
      minLength: 1,
    },
    duration_in_seconds: {
      type: 'integer',
      description:
        'Duration of this treatment in seconds. If there are no other processings whose duration is greater than this, the data field should be deleted.',
      minimum: 0,
    },
    security_policies: {
      type: 'string',
      description: 'General security policies implemented',
    },
  },
};

const processings = {
  type: 'array',
  items: processing,
};

const properties = {
  is_enabled,
  db,
  name,
  description,
  correlation_field,
  encrypted_fields,
  retry_duration,
  indexes,
  schema,
  links,
  // Model options:
  with_default_events,
  with_global_version,
  // Fully Homomorphic logic
  with_fully_homomorphic_encryption,
  fhe_public_key_field,
  // Blockchain logic
  with_blockchain_hash,
  current_hash_field,
  previous_hash_field,
  nonce_field,
  blockchain_hash_difficulty,
  blockchain_hash_genesis,
  // Performance options:
  must_wait_state_persistence,
  // GDPR
  processings,
};

const modelConfig: ModelConfig = {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  retry_duration: 0,
  indexes: [
    {
      collection: 'internal_models',
      fields: { name: 1 },
      opts: { name: 'name_unicity', unique: true },
    },
  ],
  schema: {
    model: {
      required: [],
      properties: {
        [CORRELATION_FIELD]: c.COMPONENT_CORRELATION_ID,
        ...properties,
      },
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['name', 'correlation_field', 'schema'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
      [c.EVENT_TYPE_RESTORED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
};

export default modelConfig;
