import Syncer from './Syncer';

import * as utils from './utils';

describe('sdk/Syncer', () => {
  let datastores;
  let pg;
  let client: Syncer;

  afterEach(() => {
    jest.resetAllMocks();
  });

  describe('constructor', () => {
    it('creates a new client with default configuration', () => {
      client = new Syncer({}, datastores, pg);

      expect(client.config).toEqual({});
    });

    it('creates a new client with core instanciated', () => {
      client = new Syncer({}, datastores, pg);

      // @ts-ignore
      expect(client._pg).toEqual(pg);
    });

    it('creates a new client with telemetry enabled', () => {
      client = new Syncer(
        {
          telemetry: { logger: true },
        },
        datastores,
        pg,
      );

      // @ts-ignore
      expect(client._telemetry).toEqual({ logger: true });
    });
  });

  describe('syncDataSource', () => {
    it('invokes the walkMulti function', () => {
      client = new Syncer({}, datastores, pg);

      const walkMultiMock = jest
        .spyOn(utils, 'walkMulti')
        .mockImplementation(() => Promise.resolve());

      client.syncDataSource('datastore', [], 'entities');

      expect(walkMultiMock.mock.calls[0].slice(0, 3)).toEqual([
        // @ts-ignore
        client._datastores,
        [],
        1,
      ]);
    });

    it('invokes the walkMulti function mapping all modelConfigs to trgger', () => {
      client = new Syncer({}, datastores, pg);

      const walkMultiMock = jest
        .spyOn(utils, 'walkMulti')
        .mockImplementation(() => Promise.resolve());

      client.syncDataSource(
        'datastore',

        [
          // @ts-ignore
          {
            name: 'users',
          },
          // @ts-ignore
          {
            name: 'accounts',
          },
        ],
        'entities',
      );

      expect(walkMultiMock.mock.calls[0].slice(0, 3)).toEqual([
        // @ts-ignore
        client._datastores,
        [
          {
            datastore: 'datastore',
            model: 'users',
            query: {},
            source: 'entities',
          },
          {
            datastore: 'datastore',
            model: 'accounts',
            query: {},
            source: 'entities',
          },
        ],
        1,
      ]);
    });

    it('calls the insert entity into postgreSQL', () => {
      client = new Syncer({}, datastores, pg);

      let fn;
      const walkMultiMock = jest
        .spyOn(utils, 'walkMulti')
        .mockImplementation((_datastores, triggers, pageSize, _fn) => {
          fn = _fn;

          return Promise.resolve();
        });

      client.syncDataSource(
        'datastore',

        [
          // @ts-ignore
          {
            name: 'users',
          },
        ],
        'entities',
      );

      // @ts-ignore
      client._pg = {
        insert: jest.fn(),
      };

      fn({ firstname: 'John' }, { model: 'users', source: 'entities' });

      // @ts-ignore
      expect(client._pg.insert).toHaveBeenCalledWith(
        { name: 'users' },
        'entities',
        { firstname: 'John' },
      );
    });
  });

  describe('syncData', () => {
    it('invokes the syncDataSource function', () => {
      client = new Syncer({}, datastores, pg);

      const syncDataSourceMock = jest
        .spyOn(client, 'syncDataSource')
        .mockImplementation(() => Promise.resolve());

      client.syncData('datastore', []);

      expect(syncDataSourceMock.mock.calls[0].slice(0, 3)).toEqual([
        'datastore',
        [],
        'entities',
      ]);

      expect(syncDataSourceMock.mock.calls[1].slice(0, 3)).toEqual([
        'datastore',
        [],
        'events',
      ]);
    });
  });
});
