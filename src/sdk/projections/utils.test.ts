import * as utils from './utils';

describe('utils', () => {
  describe('#main', () => {
    it('returns handler configuration', async () => {
      const config = await utils.main(new URL('https://local.org'));

      expect(config).toMatchObject({
        triggers: [
          {
            datastore: 'models',
            model: 'all',
            source: 'events',
          },
        ],
      });
    });

    it('returns services from start handler', async () => {
      const config = await utils.main(new URL('https://local.org'));

      const services = await config.start();

      expect(services).toMatchObject({
        datastores: {},
      });
    });

    it('returns void from stop handler', async () => {
      const config = await utils.main(new URL('https://local.org'));

      const res = await config.stop();

      expect(res).toBeUndefined();
    });

    it('returns void from handler handler', async () => {
      const config = await utils.main(new URL('https://local.org'));

      const res = await config.handler({}, {});

      expect(res).toBeUndefined();
    });

    it('logs progress', async () => {
      const services = {
        telemetry: {
          logger: {
            debug: jest.fn(),
            info: jest.fn(),
          },
        },
      };
      const config = await utils.main(
        new URL('https://local.org?progress=1'),
        services,
      );

      const res = await config.handler({}, {});

      expect(services.telemetry.logger.info).toHaveBeenCalledTimes(1);
    });

    it('waits for a specific time window', async () => {
      const config = await utils.main(new URL('https://local.org?timeout=100'));

      const tic = Date.now();
      const res = await config.handler({}, {});
      const diff = Date.now() - tic;

      expect(diff).toBeGreaterThanOrEqual(90);
    });

    it('blocks for a specific time window', async () => {
      const config = await utils.main(new URL('https://local.org?block=100'));

      const tic = Date.now();
      const res = await config.handler({}, {});
      const diff = Date.now() - tic;

      expect(diff).toBeGreaterThanOrEqual(100);
    });

    it('raises an exception', async () => {
      const config = await utils.main(new URL('https://local.org?exception=1'));

      let error;
      try {
        const res = await config.handler({}, {});
      } catch (err) {
        error = err;
      }

      expect(error.message).toEqual('This is an error');
    });

    it('requests the heartbeat', async () => {
      const services = {
        telemetry: {
          logger: {
            debug: () => {},
            info: () => {},
          },
        },
        datastores: new Map([['models', { heartbeat: jest.fn() }]]),
      };
      const config = await utils.main(
        new URL('https://local.org?with_heartbeat=true'),
        services,
      );

      const res = await config.handler({}, {});

      expect(
        services.datastores.get('models')?.heartbeat,
      ).toHaveBeenCalledTimes(1);
    });

    it('calls a find', async () => {
      const services = {
        telemetry: {
          logger: {
            debug: () => {},
            info: () => {},
          },
        },
        datastores: new Map([['models', { find: jest.fn() }]]),
      };
      const config = await utils.main(
        new URL('https://local.org?with_fetch=true'),
        services,
      );

      const res = await config.handler({}, {});

      expect(services.datastores.get('models')?.find).toHaveBeenCalledTimes(1);
    });
  });
});
