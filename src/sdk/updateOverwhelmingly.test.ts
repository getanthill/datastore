import type { MongoDbConnector } from '@getanthill/mongodb-connector';
import type App from '../App';

import setup from '../../test/setup';
import Datastore from './Datastore';

describe('sdk/Datastore/updateOverwhelmingly', () => {
  let mongodb: MongoDbConnector;
  let instance: App;
  let sdk: Datastore;
  const mocks: any = {};

  const properties = {
    firstname: {
      type: 'string',
    },
  };

  const modelConfigs = {
    users: {
      is_enabled: true,
      db: 'datastore',
      name: 'users',
      correlation_field: 'user_id',
      schema: {
        model: {
          type: 'object',
          additionalProperties: true,
          properties: properties,
        },
        events: {
          CREATED: {
            '0_0_0': {
              additionalProperties: true,
              properties,
            },
          },
          UPDATED: {
            '0_0_0': {
              additionalProperties: true,
              properties,
            },
          },
        },
      },
    },
  };

  beforeEach(async () => {
    mocks.processExit = jest
      .spyOn(process, 'exit')
      // @ts-ignore
      .mockImplementation(() => null);

    [, mongodb, , , , sdk, , instance] = await setup.startApi({
      mode: 'development',
      features: { api: { admin: true } },
    });
  });

  afterEach(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('#updateOverwhelmingly', () => {
    let entities;
    beforeEach(async () => {
      await sdk.createModel(modelConfigs.users);

      entities = await sdk.import([
        {
          model: 'users',
          id: 'users/john',
          idempotency: {
            firstname: 'john',
          },
          entity: {
            firstname: 'john',
          },
        },
        {
          model: 'users',
          id: 'users/jack',
          idempotency: {
            firstname: 'jack',
          },
          entity: {
            firstname: 'jack',
          },
        },
      ]);
    });

    it('update a single entity', async () => {
      const stats = await sdk.updateOverwhelmingly(
        'users',
        {
          user_id: entities.get('users/john').user_id,
        },
        async (user) => {
          return {
            new_key: true,
          };
        },
        () => null,
      );

      expect(stats).toEqual({
        total: 1,
        done: 1,
        error: 0,
        progress: 1,
        restored: 0,
      });

      const { data: john } = await sdk.get(
        'users',
        entities.get('users/john').user_id,
      );
      expect(john).toMatchObject({ version: 1, new_key: true });
    });

    it('update all entities', async () => {
      const stats = await sdk.updateOverwhelmingly(
        'users',
        {},
        async () => ({
          new_key: true,
        }),
        () => null,
      );

      expect(stats).toEqual({
        total: 2,
        done: 2,
        error: 0,
        progress: 1,
        restored: 0,
      });

      const { data } = await sdk.find('users', {});
      expect(data).toMatchObject([
        { version: 1, new_key: true },
        { version: 1, new_key: true },
      ]);
    });

    it('restores to the previous version in case of error', async () => {
      const stats = await sdk.updateOverwhelmingly(
        'users',
        {},
        async () => ({
          new_key: true,
        }),
        (stats) => {
          if (stats.done === 1) {
            throw new Error('Ooops');
          }
        },
        1, // Ensure an execition in order
      );

      expect(stats).toEqual({
        total: 2,
        done: 2,
        error: 1,
        progress: 1,
        restored: 1,
      });

      const { data } = await sdk.find('users', {});

      expect(data).toMatchObject([
        { version: 2 },
        { version: 1, new_key: true },
      ]);
    });
  });
});
