import { ErrorObject } from 'ajv';

import type { JSONSchema4 } from 'json-schema';

export interface JSONSchema extends JSONSchema4 {}

/**
 * Authorizations
 */
type AnyObject = { [key: string]: any };

export const AUTHORIZATION_VERB_ALLOW = 'allow';
export const AUTHORIZATION_VERB_DENY = 'deny';

export const AUTHORIZATION_SCOPE_ACTION = 'action';
export const AUTHORIZATION_SCOPE_CONTEXT = 'context';
export const AUTHORIZATION_SCOPE_OBJECT = 'object';
export const AUTHORIZATION_SCOPE_SUBJECT = 'subject';

export declare enum AuthorizationScopes {
  Subject = 'subject',
  Action = 'action',
  Object = 'object',
  Context = 'context',
}
export type AuthorizationScope =
  | AuthorizationScopes.Subject
  | AuthorizationScopes.Action
  | AuthorizationScopes.Object
  | AuthorizationScopes.Context;

export type Scope = Array<string>;
export interface Action {
  scope: Scope;
  [key: string]: any;
}
export interface Subject {
  scope: Scope;
  [key: string]: any;
}
export interface Object {
  scope: Scope;
  [key: string]: any;
}
export interface Context {
  scope: Scope;
  [key: string]: any;
}
export interface AuthorizationRequest {
  action: Action;
  subject: Subject;
  object: Object;
  context: Context;
}
export interface ScopeValidation {
  is_valid: boolean;
  errors: Array<ErrorObject<string, Record<string, any>, unknown>>;
}
export interface RuleValidation {
  [AuthorizationScopes.Action]: ScopeValidation;
  [AuthorizationScopes.Subject]: ScopeValidation;
  [AuthorizationScopes.Object]: ScopeValidation;
  [AuthorizationScopes.Context]: ScopeValidation;
}
export interface Decision {
  verb: PolicyVerb;
  obligations: Array<Obligation>;
  validations: Array<RuleValidation>;
}
export interface Attribute {
  attribute_id?: string;
  is_enabled: boolean;
  attribute: string;
  description: string;
  value: string;
  scope: Scope;
}
export interface Rule {
  rule_id?: string;
  is_enabled: boolean;
  name: string;
  description: string;
  subject: JSONSchema;
  action: JSONSchema;
  object: JSONSchema;
  context: JSONSchema;
}
export interface Obligation {
  obligation_id?: string;
  is_enabled: boolean;
  name: string;
  description: string;
  type: 'patch' | 'pick';
  source: 'payload' | 'query' | 'headers' | 'body';
  value: any;
}
export declare enum PolicyVerbs {
  Allow = AUTHORIZATION_VERB_ALLOW,
  Deny = AUTHORIZATION_VERB_DENY,
}
export type PolicyVerb = PolicyVerbs.Allow | PolicyVerbs.Deny;
export interface Policy {
  policy_id?: string;
  is_enabled: boolean;
  name: string;
  description: string;
  scope: Scope;
  verb: PolicyVerb;
  rules: Array<Rule>;
  obligations: Array<Obligation>;
}

export interface RequestAttributes {
  [AuthorizationScopes.Action]: Array<Attribute>;
  [AuthorizationScopes.Subject]: Array<Attribute>;
  [AuthorizationScopes.Object]: Array<Attribute>;
  [AuthorizationScopes.Context]: Array<Attribute>;
}
