import setup from '../../setup';

import thingsModelConfig from '../../../src/templates/examples/things.json';

const memwatch = require('@airbnb/node-memwatch');

describe('bench/memory/create', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let sdk;
  let uuid;
  let instance;

  beforeAll(async () => {
    [config, mongodb, models, app, , sdk, , instance] = await setup.startApi({
      mode: 'production',
      features: {
        api: {
          admin: true,
          aggregate: true,
        },
      },
    });

    config.datastores = [
      {
        name: 'datastore',
        config: {
          baseUrl: `http://localhost:${config.port}`,
          debug: false,
          token: 'token',
        },
      },
    ];

    await sdk.createModel({
      ...thingsModelConfig,
      db: 'datastore',
      name: 'projections',
      correlation_field: 'projection_id',
    });

    await instance.restart();
  });

  beforeEach(async () => {
    uuid = setup.uuid();

    await Promise.all([
      mongodb.db('datastore_write').collection('projections').deleteMany({}),
    ]);
  });

  afterEach(async () => {
    sdk.closeAll();

    await new Promise((resolve) => setTimeout(resolve, 100));
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  it('does not increase the memory footprint', async () => {
    await sdk.create('projections', {
      firstname: `alice:${uuid}`,
    });

    const hd = new memwatch.HeapDiff();

    for (let i = 0; i < 100; i++) {
      await sdk.aggregate([
        {
          type: 'fetch',
          datastore: 'datastore',
          model: 'projections',
          destination: 'entity',
          as_entity: true,
          query: {
            firstname: `alice:${uuid}`,
          },
        },
        {
          type: 'fetch',
          datastore: 'datastore',
          model: 'projections',
          source: 'events',
          destination: 'events',
          map: [
            {
              from: 'entity.projection_id',
              to: 'projection_id',
            },
          ],
        },
        {
          type: 'op',
          func: 'length',
          path: 'events',
          destination: 'events_count',
        },
      ]);
    }

    const diff = hd.end();

    expect(
      (diff.after.size_bytes - diff.before.size_bytes) / diff.before.size_bytes,
    ).toBeLessThanOrEqual(0.05);
  });
});
