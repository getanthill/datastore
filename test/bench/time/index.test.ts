import setup from '../../setup';

import thingsModelConfig from '../../../src/templates/examples/things.json';

describe('bench/time', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk;
  let uuid;
  let instance;

  beforeAll(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'production',
        features: {
          api: {
            admin: true,
          },
        },
      });

    await sdk.createModel({
      ...thingsModelConfig,
      db: 'datastore',
      name: 'projections',
      correlation_field: 'projection_id',
    });

    await instance.restart();
  });

  beforeEach(async () => {
    uuid = setup.uuid();

    await Promise.all([
      mongodb.db('datastore_write').collection('projections').deleteMany({}),
    ]);
  });

  afterEach(async () => {
    sdk.closeAll();

    await new Promise((resolve) => setTimeout(resolve, 100));
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  it('can call 1000 times heartbeat in less than 1.6s (1.7ms/req)', async () => {
    const tic = Date.now();

    await Promise.all(new Array(1000).fill(1).map(() => sdk.heartbeat()));

    const tac = Date.now();

    expect(tac - tic).toBeLessThanOrEqual(1700);
  });

  it('can call 1000 times heartbeat without any response time dispersion', async () => {
    const times = [];

    for (let i = 0; i < 1000; i++) {
      const tic = Date.now();
      await sdk.heartbeat();

      const tac = Date.now();
      times.push(tac - tic);
    }

    expect(Math.max(...times) - Math.min(...times)).toBeLessThanOrEqual(25);
  });

  it('can get one entity 1000 times in less than 3s (30ms/req)', async () => {
    const { data: entity } = await sdk.create('projections', {
      firstname: `alice:${uuid}`,
    });

    const tic = Date.now();

    await Promise.all(
      new Array(1000)
        .fill(1)
        .map(() => sdk.get('projections', entity.projection_id)),
    );

    const tac = Date.now();

    expect(tac - tic).toBeLessThanOrEqual(3000);
  });

  it('can find one entity 1000 times in less than 3s (30ms/req)', async () => {
    const { data: entity } = await sdk.create('projections', {
      firstname: `alice:${uuid}`,
    });

    const tic = Date.now();

    await Promise.all(
      new Array(1000).fill(1).map(() =>
        sdk.find('projections', {
          firstname: `alice:${uuid}`,
        }),
      ),
    );

    const tac = Date.now();

    expect(tac - tic).toBeLessThanOrEqual(3000);
  });

  it('can create one entity in less than 100ms', async () => {
    const tic = Date.now();
    await sdk.create('projections', {
      firstname: `alice:${uuid}`,
    });

    const tac = Date.now();

    expect(tac - tic).toBeLessThanOrEqual(100);
  });

  it.only('can create 1000 entities in less than 5s', async () => {
    const tic = Date.now();

    await Promise.all(
      new Array(1000).fill(1).map(() =>
        sdk.create('projections', {
          firstname: `alice:${uuid}`,
        }),
      ),
    );

    const tac = Date.now();

    expect(tac - tic).toBeLessThanOrEqual(5000);
  });

  it('can create 2000 entities in less than 7s', async () => {
    const tic = Date.now();

    await Promise.all(
      new Array(2000).fill(1).map(() =>
        sdk.create('projections', {
          firstname: `alice:${uuid}`,
        }),
      ),
    );

    const tac = Date.now();

    expect(tac - tic).toBeLessThanOrEqual(7000);
  });
});
