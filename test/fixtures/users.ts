import type { ModelConfig } from '../../src/typings';

import * as c from '../../src/constants';

const CORRELATION_FIELD = 'user_id';

const is_enabled = {
  ...c.COMPONENTS.is_enabled,
  default: true,
};

const is_readonly = {
  ...c.COMPONENTS.is_readonly,
};

const is_archived = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity archived',
};

const is_deleted = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity deleted',
};

const firstname = {
  ...c.COMPONENT_STRING,
  description: 'Firstname of the user',
  example: 'John',
};

const lastname = {
  ...c.COMPONENT_STRING,
  description: 'Lastname of the user',
  example: 'Doe',
};

const email = {
  ...c.COMPONENT_EMAIL,
  description: 'Email of the user',
  example: 'john@doe.org',
};

const age = {
  ...c.COMPONENT_INTEGER,
  description: 'User age',
  example: 33,
};

const phone = {
  type: 'object',
  properties: {
    number: {
      type: 'string',
    },
  },
};

const count = {
  ...c.COMPONENT_INTEGER,
};

const last_seen = {
  ...c.COMPONENT_DATE,
};

const sensitive_data = {
  ...c.COMPONENT_STRING,
  description: 'Sensitive encrypted data',
};

const properties = {
  is_enabled,
  is_readonly,
  is_archived,
  is_deleted,
  firstname,
  lastname,
  phone,
  email,
  age,
  count,
  last_seen,
  nested: {
    type: 'object',
    properties: {
      birth_date: c.COMPONENT_DATE,
    },
  },
  sensitive_data,
};

const modelConfig: ModelConfig = {
  is_enabled: true,
  db: 'datastore',
  name: 'users',
  correlation_field: CORRELATION_FIELD,
  encrypted_fields: ['sensitive_data'],
  retry_duration: 5000,
  indexes: [
    {
      collection: 'users',
      fields: { email: 1 },
      opts: { name: 'email', unique: true, sparse: true },
    },
  ],
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['firstname'],
        },
      },
      [c.EVENT_TYPE_ARCHIVED]: {
        '0_0_0': {
          type: 'object',
          additionalProperties: false,
          properties,
        },
      },
      [c.EVENT_TYPE_DELETED]: {
        '0_0_0': {
          type: 'object',
          additionalProperties: true,
        },
      },
      FIRSTNAME_UPDATED: {
        '0_0_0': {
          required: ['firstname'],
          additionalProperties: false,
          properties: {
            firstname,
          },
        },
      },
      EMAIL_UPDATED: {
        '0_0_0': {
          required: ['email'],
          additionalProperties: false,
          properties: {
            email,
          },
        },
      },
    },
  },
  processings: [
    {
      field: 'firstname',
      name: 'Say hello',
      purpose:
        'The objective of this processing is to access the firstname of the user to say hello politely.',
      persons: ['users'],
      recipients: ['anthill'],
      tokens: ['decrypt'],
      duration_in_seconds: 31536000,
    },
  ],
};

export default modelConfig;
