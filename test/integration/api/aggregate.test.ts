import request from 'supertest';
import express from 'express';

import api from '../../../src/api';

import setup from '../../setup';

describe('/api/aggregate', () => {
  let mongodb;
  let models;
  let app;
  let services;

  beforeEach(async () => {
    app = await setup.build({
      mode: 'development',
      security: {
        tokens: [
          {
            id: 'read',
            level: 'read',
            token: 'token',
          },
        ],
      },
      features: {
        api: {
          admin: true,
          aggregate: true,
        },
        fhe: {
          isEnabled: true,
        },
      },
    });

    services = app.services;
    mongodb = services.mongodb;

    models = await setup.initModels(services, [
      {
        is_enabled: true,
        db: 'datastore',
        name: 'accounts',
        correlation_field: 'account_id',
        schema: {
          model: {
            type: 'object',
            properties: {
              firstname: { type: 'string' },
              data: {
                type: 'string',
              },
              pub: {
                type: 'string',
              },
            },
          },
        },
      },
    ]);

    app = express();

    app
      .get('/heartbeat', (_req, res): void => {
        res.json({ is_alive: true });
      })
      .use('/api', await api({ ...services, models }));
  });

  afterEach(async () => {
    await setup.teardownDb(mongodb);
  });

  it('returns the aggregate', async () => {
    const res = await request(app)
      .post('/api/aggregate')
      .set('Accept', 'application/json')
      .set(
        'Authorization',
        services.config.security.tokens.filter(
          (access) => access.level === 'read',
        )[0].token,
      )
      .send([]);

    expect(res.body).toMatchObject({});
  });

  it('returns an invalid pipeline definition if FHE is not enabled', async () => {
    // Given
    services.config.features.fhe.isEnabled = false;

    const uuid = setup.uuid();
    const value = services.fhe.createCypher([1]).save();
    const publicKey = services.fhe.keys.public.save();

    const account = await models.factory('accounts').create({
      firstname: `Alice ${uuid}`,
      data: value,
    });

    const res = await request(app)
      .post('/api/aggregate')
      .set('Accept', 'application/json')
      .set(
        'Authorization',
        services.config.security.tokens.filter(
          (access) => access.level === 'read',
        )[0].token,
      )
      .send({
        pipeline: [
          {
            type: 'fhe',
            name: 'FHE increment example',
            script: async function handler(a) {
              return a + 1;
            }.toString(),
            args: ['account.data'],
          },
        ],
        data: {
          public_key: publicKey,
          account: account.state,
        },
      });

    expect(res.body).toEqual({
      status: 400,
      details: [],
      message: 'Invalid pipeline definition',
    });
  });

  it('performs FHE computations in aggregation', async () => {
    // Given
    const uuid = setup.uuid();
    const value = services.fhe.createCypher([1]).save();
    const publicKey = services.fhe.keys.public.save();

    const account = await models.factory('accounts').create({
      firstname: `Alice ${uuid}`,
      data: value,
    });

    const res = await request(app)
      .post('/api/aggregate')
      .set('Accept', 'application/json')
      .set(
        'Authorization',
        services.config.security.tokens.filter(
          (access) => access.level === 'read',
        )[0].token,
      )
      .send({
        pipeline: [
          {
            type: 'fhe',
            name: 'FHE increment example',
            script: async function handler(a) {
              return a + 1;
            }.toString(),
            args: ['account.data'],
            destination: 'account.data',
          },
        ],
        data: {
          public_key: publicKey,
          account: account.state,
        },
      });

    const cipher = services.fhe.seal.CipherText();
    cipher.load(services.fhe.context, res.body.account.data);
    expect(services.fhe.fromCypher(cipher).slice(0, 1)).toEqual([2]);
  });

  it('performs FHE computations in aggregation with a different public key location', async () => {
    // Given
    const uuid = setup.uuid();
    const value = services.fhe.createCypher([1]).save();
    const publicKey = services.fhe.keys.public.save();

    const account = await models.factory('accounts').create({
      firstname: `Alice ${uuid}`,
      data: value,
      pub: publicKey,
    });

    const res = await request(app)
      .post('/api/aggregate')
      .set('Accept', 'application/json')
      .set(
        'Authorization',
        services.config.security.tokens.filter(
          (access) => access.level === 'read',
        )[0].token,
      )
      .send({
        pipeline: [
          {
            type: 'fhe',
            name: 'FHE increment example',
            script: async function handler(a) {
              return a + 1;
            }.toString(),
            args: ['account.data'],
            public_key: 'account.pub',
            destination: 'account.data',
          },
        ],
        data: {
          account: account.state,
        },
      });

    const cipher = services.fhe.seal.CipherText();
    cipher.load(services.fhe.context, res.body.account.data);
    expect(services.fhe.fromCypher(cipher).slice(0, 1)).toEqual([2]);
  });

  it('performs FHE computations in aggregation with a public key provided in the step definition', async () => {
    // Given
    const uuid = setup.uuid();
    const value = services.fhe.createCypher([1]).save();
    const publicKey = services.fhe.keys.public.save();

    const account = await models.factory('accounts').create({
      firstname: `Alice ${uuid}`,
      data: value,
    });

    const res = await request(app)
      .post('/api/aggregate')
      .set('Accept', 'application/json')
      .set(
        'Authorization',
        services.config.security.tokens.filter(
          (access) => access.level === 'read',
        )[0].token,
      )
      .send({
        pipeline: [
          {
            type: 'fhe',
            name: 'FHE increment example',
            script: async function handler(a) {
              return a + 1;
            }.toString(),
            args: ['account.data'],
            public_key: publicKey,
            destination: 'account.data',
          },
        ],
        data: {
          account: account.state,
        },
      });

    const cipher = services.fhe.seal.CipherText();
    cipher.load(services.fhe.context, res.body.account.data);
    expect(services.fhe.fromCypher(cipher).slice(0, 1)).toEqual([2]);
  });

  it('performs FHE computations in aggregation without destination', async () => {
    // Given
    const uuid = setup.uuid();
    const value = services.fhe.createCypher([1]).save();
    const publicKey = services.fhe.keys.public.save();

    const account = await models.factory('accounts').create({
      firstname: `Alice ${uuid}`,
      data: value,
    });

    const res = await request(app)
      .post('/api/aggregate')
      .set('Accept', 'application/json')
      .set(
        'Authorization',
        services.config.security.tokens.filter(
          (access) => access.level === 'read',
        )[0].token,
      )
      .send({
        pipeline: [
          {
            type: 'fhe',
            name: 'FHE increment example',
            script: async function handler(a) {
              return a + 1;
            }.toString(),
            args: ['account.data'],
          },
        ],
        data: {
          public_key: publicKey,
          account: account.state,
        },
      });

    const cipher = services.fhe.seal.CipherText();
    cipher.load(services.fhe.context, res.body.fhe);
    expect(services.fhe.fromCypher(cipher).slice(0, 1)).toEqual([2]);
  });
});
