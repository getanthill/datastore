import request from 'supertest';
import express from 'express';

import api from '../../../src/api';

import fixtureUsers from '../../../test/fixtures/users';

import setup from '../../setup';

describe('/api', () => {
  let mongodb;
  let models;
  let app;
  let services;

  beforeEach(async () => {
    app = await setup.build();

    services = app.services;
    mongodb = services.mongodb;

    models = await setup.initModels(services, [fixtureUsers]);

    app = express();

    app
      .get('/heartbeat', (_req, res): void => {
        res.json({ is_alive: true });
      })
      .use('/api', await api({ ...services, models }));
  });

  afterEach(async () => {
    await setup.teardownDb(mongodb);
  });

  it('answers on the heartbeat', async () => {
    const res = await request(app).get('/heartbeat');

    expect(res.statusCode).toEqual(200);
    expect(res.body).toEqual({
      is_alive: true,
    });
  });

  it('blocks requests on internal models', async () => {
    const res = await request(app).get('/api/internal_models');

    expect(res.statusCode).toEqual(403);
    expect(res.body).toEqual({
      status: 403,
      message: 'Protected model',
      details: [],
    });
  });

  it('blocks unauthenticated requests on a valid model', async () => {
    const res = await request(app).get('/api/users');

    expect(res.statusCode).toEqual(401);
    expect(res.body).toEqual({
      status: 401,
      message: 'Unauthenticated',
      details: [],
    });
  });

  describe('Users model', () => {
    it('returns users models on authenticated request', async () => {
      const res = await request(app)
        .get('/api/users')
        .set('Authorization', 'token');

      expect(res.statusCode).toEqual(200);
      expect(res.body).toEqual([]);
    });
  });
});
