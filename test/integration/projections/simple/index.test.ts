import * as validator from '../../../../src/sdk/projections/validator';

import data from './index';

describe('scripts/projections/simple', () => {
  const DEFAULT_CONTEXT: any = {
    data,
  };

  let ctx: any = {};

  beforeEach(async () => {
    ctx = await validator.buildContext(DEFAULT_CONTEXT);
  });

  afterEach(async () => {
    await validator.tearDown(ctx);
  });

  it('allows to replay events', async () => {
    await validator.run(ctx, 'replay');

    await validator.assert(ctx, 'replay');
  });

  it('allows to stream events', async () => {
    await validator.run(ctx, 'start');

    await validator.assert(ctx, 'start');
  });
});
