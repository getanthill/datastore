import setup from '../../setup';

import thingsModelConfig from '../../../src/templates/examples/things.json';
import { Datastore } from '../../../src/sdk';

import AMQPClient from '../../../src/services/amqp';

describe('integration/stream', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk: Datastore;
  let uuid;
  let instance;

  beforeAll(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'development',
        features: {
          api: {
            admin: true,
          },
          amqp: {
            isEnabled: true,
          },
        },
      });

    await sdk.createModel({
      ...thingsModelConfig,
      db: 'datastore',
      name: 'projections',
      correlation_field: 'projection_id',
    });

    await instance.restart();
  });

  beforeEach(async () => {
    uuid = setup.uuid();

    await Promise.all([
      mongodb.db('datastore_write').collection('projections').deleteMany({}),
    ]);
  });

  afterEach(async () => {
    sdk.streams.closeAll();

    await new Promise((resolve) => setTimeout(resolve, 100));
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('AMQP', () => {
    beforeEach(async () => {
      sdk.streams.config.amqp = {
        ...instance.services.config.amqp,
        queue: {
          ...instance.services.config.amqp.queue,
          consumer: {
            ...instance.services.config.amqp.queue.consumer,
            name: 'worker',
          },
        },
      };

      sdk.streams.config.connector = 'amqp';

      const amqp = new AMQPClient(sdk.streams.config.amqp, sdk.telemetry);

      await amqp.connect();
      await amqp.init();

      await amqp._channel.purgeQueue(
        sdk.streams.config.amqp.queue.consumer.name,
      );

      await amqp.end();
    });

    afterEach(async () => {
      sdk.streams.closeAll();
    });

    it('can stream an entity creation', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {});

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('projections', 'entities', {});

      const { data: projection } = await sdk.create('projections', {
        firstname: `alice:${uuid}`,
      });

      const entity = await wait;

      expect(entity).toMatchObject(projection);

      const amqp = new AMQPClient(sdk.streams.config.amqp, sdk.telemetry);

      await amqp.connect();

      await amqp._channel.unbindQueue(
        sdk.streams.config.amqp.queue.consumer.name,
        sdk.streams.config.amqp.exchange.consumer.name,
        'projections.*.success.*',
      );

      await amqp.end();
    });

    it('can stream the events of an entity creation', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'events', {});

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('projections', 'events', {});

      const { data: projection } = await sdk.create('projections', {
        firstname: `alice:${uuid}`,
      });

      const event = await wait;

      expect(event).toMatchObject({
        firstname: `alice:${uuid}`,
        projection_id: projection.projection_id,
        type: 'CREATED',
        v: '0_0_0',
        version: 0,
      });

      const amqp = new AMQPClient(sdk.streams.config.amqp, sdk.telemetry);

      await amqp.connect();

      await amqp._channel.unbindQueue(
        sdk.streams.config.amqp.queue.consumer.name,
        sdk.streams.config.amqp.exchange.consumer.name,
        'projections.*.success.*',
      );

      await amqp.end();
    });

    it('can stream an entity creation from `all` model input', async () => {
      const streamId = sdk.streams.getStreamId('all', 'entities', {});

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('all', 'entities', {});

      const { data: projection } = await sdk.create('projections', {
        firstname: `alice:${uuid}`,
      });

      const entity = await wait;

      expect(entity).toMatchObject(projection);

      const amqp = new AMQPClient(sdk.streams.config.amqp, sdk.telemetry);

      await amqp.connect();

      await amqp._channel.unbindQueue(
        sdk.streams.config.amqp.queue.consumer.name,
        sdk.streams.config.amqp.exchange.consumer.name,
        '*.*.success.*',
      );

      await amqp.end();
    });

    it('can stream multiple entity creations', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {});

      let entities = [];
      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, (entity) => {
          entities.push(entity);

          if (entities.length === 10) {
            resolve(entities);
          }
        });
      });

      await sdk.streams.listen('projections', 'entities', {});

      /**
       * Order of reception is obtained thanks to
       * the await in the following loop
       */
      for (let i = 0; i < 10; i++) {
        await sdk.create('projections', {
          firstname: `${i}:${uuid}`,
        });
      }

      await wait;

      expect(entities.length).toEqual(10);

      expect(entities.map((e) => e.firstname)).toEqual(
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((a) => `${a}:${uuid}`),
      );

      const amqp = new AMQPClient(sdk.streams.config.amqp, sdk.telemetry);

      await amqp.connect();

      await amqp._channel.unbindQueue(
        sdk.streams.config.amqp.queue.consumer.name,
        sdk.streams.config.amqp.exchange.consumer.name,
        'projections.*.success.*',
      );

      await amqp.end();
    });

    it('can receive a huge entity', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {});

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('projections', 'entities', {});

      const payload = {
        firstname: `eve:${uuid}`,
      };

      for (let i = 0; i < 4000; i++) {
        payload[`key-${i}`] = 'Lorem Ipsum';
      }

      await sdk.create('projections', payload);

      const entity = await wait;

      expect(entity).toMatchObject(payload);
    });

    it('throws an exception on very very huge entities', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {});

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('projections', 'entities', {});

      const payload = {
        firstname: `eve:${uuid}`,
      };

      for (let i = 0; i < 50000; i++) {
        payload[`key-${i}`] = 'Lorem Ipsum';
      }

      await sdk.create('projections', payload);

      const entity = await wait;

      expect(entity).toMatchObject(payload);
    });
  });
});
