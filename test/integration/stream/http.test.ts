import setup from '../../setup';

import thingsModelConfig from '../../../src/templates/examples/things.json';
import { Datastore } from '../../../src/sdk';

describe('integration/stream', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk: Datastore;
  let uuid;
  let instance;

  beforeAll(async () => {
    [config, mongodb, models, app, server, sdk, , instance] =
      await setup.startApi({
        mode: 'development',
        features: {
          api: {
            admin: true,
          },
        },
      });

    await sdk.createModel({
      ...thingsModelConfig,
      db: 'datastore',
      name: 'projections',
      correlation_field: 'projection_id',
    });

    await instance.restart();
  });

  beforeEach(async () => {
    uuid = setup.uuid();

    await Promise.all([
      mongodb.db('datastore_write').collection('projections').deleteMany({}),
    ]);
  });

  afterEach(async () => {
    sdk.streams.closeAll();

    await new Promise((resolve) => setTimeout(resolve, 100));
  });

  afterAll(async () => {
    await setup.teardownDb(mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('HTTP', () => {
    it('can stream an entity creation', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {
        firstname: `alice:${uuid}`,
      });

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('projections', 'entities', {
        firstname: `alice:${uuid}`,
      });

      const { data: projection } = await sdk.create('projections', {
        firstname: `alice:${uuid}`,
      });

      const entity = await wait;

      expect(entity).toMatchObject(projection);
    });

    it('can stream multiple entity creations', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {});

      let entities = [];
      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, (entity) => {
          entities.push(entity);

          if (entities.length === 10) {
            resolve(entities);
          }
        });
      });

      await sdk.streams.listen('projections', 'entities', {});

      /**
       * Order of reception is obtained thanks to
       * the await in the following loop
       */
      for (let i = 0; i < 10; i++) {
        await sdk.create('projections', {
          firstname: `${i}:${uuid}`,
        });
      }

      await wait;

      expect(entities.length).toEqual(10);

      expect(entities.map((e) => e.firstname)).toEqual(
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9].map((a) => `${a}:${uuid}`),
      );
    });

    it('can receive a huge entity', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {});

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('projections', 'entities', {});

      const payload = {
        firstname: `eve:${uuid}`,
      };

      for (let i = 0; i < 4000; i++) {
        payload[`key-${i}`] = 'Lorem Ipsum';
      }

      await sdk.create('projections', payload);

      const entity = await wait;

      expect(entity).toMatchObject(payload);
    });

    it('throws an exception on very very huge entities', async () => {
      const streamId = sdk.streams.getStreamId('projections', 'entities', {});

      const wait = new Promise((resolve) => {
        sdk.streams.on(streamId, resolve);
      });

      await sdk.streams.listen('projections', 'entities', {});

      const payload = {
        firstname: `eve:${uuid}`,
      };

      for (let i = 0; i < 50000; i++) {
        payload[`key-${i}`] = 'Lorem Ipsum';
      }

      await sdk.create('projections', payload);

      const entity = await wait;

      expect(entity).toMatchObject(payload);
    });
  });
});
