import * as c from '../../../../../src/constants';

import { COMPONENT_CORRELATION_ID } from '../../../../../src/constants/components';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'profiles';
const CORRELATION_FIELD = 'profile_id';

const is_enabled = c.COMPONENTS.is_enabled;

const is_readonly = {
  ...c.COMPONENTS.is_readonly,
};

const is_archived = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity archived',
};

const is_deleted = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity deleted',
};

const name = {
  ...c.COMPONENT_STRING,
  description: 'Name of the profile',
  example: 'Alice Profile',
};

const account_id = {
  ...COMPONENT_CORRELATION_ID,
  description: 'Account ID',
};

const properties = {
  is_enabled,
  is_readonly,
  is_archived,
  is_deleted,
  account_id,
  name,
};

export default {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  encrypted_fields: ['name'],
  indexes: [
    {
      collection: MODEL_NAME,
      fields: { 'name::hash': 1 },
      opts: { name: 'name_hash', unique: true, sparse: true },
    },
  ],
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['name'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
};
