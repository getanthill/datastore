import * as c from '../../../../../src/constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'accounts';
const CORRELATION_FIELD = 'account_id';

const is_enabled = c.COMPONENTS.is_enabled;

const is_readonly = {
  ...c.COMPONENTS.is_readonly,
};

const is_archived = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity archived',
};

const is_deleted = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity deleted',
};

const name = {
  ...c.COMPONENT_STRING,
  description: 'Name of the account',
  example: 'Alice',
};

const reset_password_uuid = {
  ...c.COMPONENT_UUID_V4,
  description: 'Unique ID to use to reset the password',
};

const reset_password_expires_by = {
  ...c.COMPONENT_DATE,
  description: 'Reset password request expiration date',
};

const properties = {
  is_enabled,
  is_readonly,
  is_archived,
  is_deleted,
  name,
  // Reset password
  reset_password_uuid,
  reset_password_expires_by,
};

export default {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  encrypted_fields: ['name'],
  indexes: [
    {
      collection: MODEL_NAME,
      fields: { 'name::hash': 1 },
      opts: { name: 'name_hash', unique: true, sparse: true },
    },
  ],
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['name'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
    },
  },
};
