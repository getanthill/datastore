import crypto from 'crypto';

import setup from '../../../setup';

import * as modelConfigs from './models';

describe('integration/use-cases/accounts', () => {
  let config;
  let mongodb;
  let models;
  let app;
  let server;
  let sdk;
  let instance;

  beforeEach(async () => {
    try {
      [config, mongodb, models, app, server, sdk, , instance] =
        await setup.startApi({
          mode: 'production',
          security: {
            encryptionKeys: {
              all: [crypto.randomBytes(16).toString('hex')],
              archive: [crypto.randomBytes(16).toString('hex')],
            },
          },
          features: {
            deleteAfterArchiveDurationInSeconds: 0,
            api: {
              admin: true,
              graphql: false,
              updateSpecOnModelsChange: true,
              openAPI: {
                isEnabled: false,
              },
            },
            initInternalModels: false,
          },
        });
    } catch (err) {
      console.error(err);
    }
  });

  afterEach(async () => {
    await setup.teardownDb(instance.services.mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('functional', () => {
    beforeEach(async () => {
      await sdk.createModel(modelConfigs.accounts);

      await instance.restart();

      await sdk.createModelIndexes(modelConfigs.accounts);
    });

    it('allows the creation of a new account', async () => {
      const { data: alice } = await sdk.create('accounts', {
        name: 'Alice',
      });

      expect(alice?.name?.hash).toEqual(
        'c1a518e720e60b55d4dc467def2b6cc1893444a206d2566d3cbb5a72cae52106df1bd33035581392a7cb55e16b99d6090402d2642a86da87d1c8f5d7cec3f45e',
      );

      const { data: accounts } = await sdk.find('accounts', {
        name: 'Alice',
        _must_hash: true,
      });

      expect(accounts).toMatchObject([
        {
          account_id: alice.account_id,
        },
      ]);
    });
  });
});
