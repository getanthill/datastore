import crypto from 'crypto';

import type { DatastoreConfig, Services } from '../../../../src/typings';
import type { Express } from 'express';
import type { Server } from 'http';
import type { Models } from '../../../../src/models';
import type { MongoDbConnector } from '@getanthill/mongodb-connector';
import type App from '../../../../src/App';
import type { Datastore } from '../../../../src/sdk';

import setup from '../../../setup';

import * as modelConfigs from './models';

describe('integration/use-cases/fhe (Fully Homomorphic Encryption)', () => {
  let config: DatastoreConfig;
  let mongodb: MongoDbConnector;
  let models: Models;
  let app: Express | null;
  let server: Server | null;
  let sdk: Datastore;
  let services: Services;
  let instance: App;

  beforeEach(async () => {
    [config, mongodb, models, app, server, sdk, services, instance] =
      await setup.startApi({
        mode: 'production',
        security: {
          encryptionKeys: {
            all: [crypto.randomBytes(16).toString('hex')],
            archive: [crypto.randomBytes(16).toString('hex')],
          },
        },
        features: {
          deleteAfterArchiveDurationInSeconds: 0,
          api: {
            admin: true,
            graphql: true,
            updateSpecOnModelsChange: true,
          },
          initInternalModels: false,
          fhe: {
            isEnabled: true,
          },
        },
      });
  });

  afterEach(async () => {
    await setup.teardownDb(instance.services.mongodb);

    await setup.stopApi(instance);

    jest.restoreAllMocks();
  });

  describe('functional', () => {
    beforeEach(async () => {
      await sdk.createModel(modelConfigs.metrics);

      await instance.restart();
    });

    it('allows the creation of a new metric', async () => {
      // Given
      const value = services.fhe.createCypher([1, 2, 3]).save();
      const publicKey = services.fhe.keys!.public.save();

      const { data: heartbeat } = await sdk.create('metrics', {
        name: 'Heartbeat',
        public_key: publicKey,
        value,
      });

      expect(heartbeat?.name).toEqual('Heartbeat');

      const cipher = services.fhe.seal.CipherText();
      cipher.load(services.fhe.context!, heartbeat.value);
      expect(services.fhe.fromCypher(cipher).slice(0, 3)).toEqual([1, 2, 3]);
    });

    it('allows to apply increment logic on value', async () => {
      // Given
      const value = services.fhe.createCypher([1, 2, 3]).save();
      const publicKey = services.fhe.keys!.public.save();

      const { data: heartbeat } = await sdk.create('metrics', {
        name: 'Heartbeat',
        public_key: publicKey,
        value,
        count: 0,
      });

      expect(heartbeat?.name).toEqual('Heartbeat');

      const { data: incremented } = await sdk.apply(
        'metrics',
        heartbeat.metric_id,
        'INCREMENTED',
        '0_0_0',
        {},
      );

      const cipher = services.fhe.seal.CipherText();
      cipher.load(services.fhe.context!, incremented.value);
      expect(services.fhe.fromCypher(cipher).slice(0, 3)).toEqual([2, 3, 4]);
    });

    it('allows to apply increment logic on value with public key from event', async () => {
      // Given
      const value = services.fhe.createCypher([1, 2, 3]).save();
      const publicKey = services.fhe.keys!.public.save();

      const { data: heartbeat } = await sdk.create('metrics', {
        name: 'Heartbeat',
        value,
      });

      expect(heartbeat?.name).toEqual('Heartbeat');

      const { data: incremented } = await sdk.apply(
        'metrics',
        heartbeat.metric_id,
        'INCREMENTED',
        '0_0_0',
        {
          public_key: publicKey,
        },
      );

      const cipher = services.fhe.seal.CipherText();
      cipher.load(services.fhe.context!, incremented.value);
      expect(services.fhe.fromCypher(cipher).slice(0, 3)).toEqual([2, 3, 4]);
    });

    it('allows to add a value', async () => {
      // Given
      const value = services.fhe.createCypher([1, 2, 3]).save();
      const publicKey = services.fhe.keys!.public.save();

      const { data: heartbeat } = await sdk.create('metrics', {
        name: 'Heartbeat',
        public_key: publicKey,
        value,
      });

      expect(heartbeat?.name).toEqual('Heartbeat');

      const { data: incremented } = await sdk.apply(
        'metrics',
        heartbeat.metric_id,
        'ADDED',
        '0_0_0',
        {
          value: services.fhe.createCypher([4, 3, 2, 1]).save(),
        },
      );

      const cipher = services.fhe.seal.CipherText();
      cipher.load(services.fhe.context!, incremented.value);
      expect(services.fhe.fromCypher(cipher).slice(0, 4)).toEqual([5, 5, 5, 1]);
    });

    it('allows to update the metric name without FHE enabled', async () => {
      // Given
      const value = services.fhe.createCypher([1, 2, 3]).save();
      const publicKey = services.fhe.keys!.public.save();

      const { data: heartbeat } = await sdk.create('metrics', {
        name: 'Heartbeat',
        public_key: publicKey,
        value,
      });

      expect(heartbeat?.name).toEqual('Heartbeat');

      const { data: updated } = await sdk.apply(
        'metrics',
        heartbeat.metric_id,
        'NAME_UPDATED',
        '0_0_0',
        {
          name: 'Heartbeat updated',
        },
      );

      expect(updated.name).toEqual('Heartbeat updated');
    });
  });
});
