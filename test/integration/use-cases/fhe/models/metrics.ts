import * as c from '../../../../../src/constants';
import { fnToHandler } from '../../../../../src/sdk';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'metrics';
const CORRELATION_FIELD = 'metric_id';

const is_enabled = c.COMPONENTS.is_enabled;

const is_readonly = {
  ...c.COMPONENTS.is_readonly,
};

const is_archived = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity archived',
};

const is_deleted = {
  ...c.COMPONENTS.is_readonly,
  description: 'Is this entity deleted',
};

const name = {
  ...c.COMPONENT_STRING,
  description: 'Name of the account',
  example: 'Alice',
};

const public_key = {
  ...c.COMPONENT_STRING,
  description: 'Public Key',
};

const value = {
  ...c.COMPONENT_STRING,
  description: 'Fully Homomorphic Data',
};

const count = {
  ...c.COMPONENT_INTEGER,
  description: 'Count of updates',
};

const properties = {
  is_enabled,
  is_readonly,
  is_archived,
  is_deleted,
  name,
  public_key,
  value,
  count,
};

export default {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  with_fully_homomorphic_encryption: true,
  fhe_public_key_field: 'public_key',
  indexes: [
    {
      collection: `${MODEL_NAME}_events`,
      fields: { created_at: 1 },
      opts: { name: 'created_at_ttl', expireAfterSeconds: 0 }, // Remove the entity after 1 year
    },
  ],
  schema: {
    model: {
      properties,
    },
    events: {
      [c.EVENT_TYPE_CREATED]: {
        '0_0_0': {
          required: ['name'],
          properties,
        },
      },
      [c.EVENT_TYPE_UPDATED]: {
        '0_0_0': {
          properties,
        },
      },
      INCREMENTED: {
        '0_0_0': {
          properties: {
            public_key: {
              type: 'string',
              description: 'Optional public key attached',
            },
          },
          handlers: [
            {
              is_fhe: true,
              handler: fnToHandler((state, event) => {
                state.value += 1;

                return [state];
              }),
            },
            {
              is_fhe: false,
              handler: fnToHandler((state, event) => {
                state.count = (state.count ?? 0) + 1;

                return [state];
              }),
            },
          ],
        },
      },
      ADDED: {
        '0_0_0': {
          is_fhe: true,
          properties: {
            value: {
              type: 'string',
              description: 'Cypher text to add on value',
            },
            public_key: {
              type: 'string',
              description: 'Optional public key attached',
            },
          },
          handler: fnToHandler((state, event) => {
            state.value += event.value;

            return [state];
          }),
        },
      },
      NAME_UPDATED: {
        '0_0_0': {
          properties: {
            name: {
              type: 'string',
            },
          },
          handler: fnToHandler((state, event) => {
            state.name = event.name;

            return [state];
          }),
        },
      },
    },
  },
};
