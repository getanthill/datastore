import * as c from '../../../../../src/constants';

const MODEL_DATABASE = 'datastore';
const MODEL_NAME = 'lists';
const CORRELATION_FIELD = 'list_id';

const is_enabled = c.COMPONENTS.is_enabled;

const name = {
  ...c.COMPONENT_STRING,
  description: 'Name of the list item',
  example: 'Drink less coffee',
};

const properties = {
  is_enabled,
  name,
};

export default {
  is_enabled: true,
  db: MODEL_DATABASE,
  name: MODEL_NAME,
  correlation_field: CORRELATION_FIELD,
  indexes: [
    {
      collection: MODEL_NAME,
      fields: { name: 1 },
      opts: { name: 'name', unique: true },
    },
  ],
  schema: {
    model: {
      properties,
    },
  },
};
